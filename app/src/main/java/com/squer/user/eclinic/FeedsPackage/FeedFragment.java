package com.squer.user.eclinic.FeedsPackage;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.squer.user.eclinic.Entity.FeedDataPackage.FeedData;
import com.squer.user.eclinic.R;
import com.squer.user.eclinic.Rest.RestClient;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user on 11/30/2017.
 */

public class FeedFragment extends Fragment {
    private HashMap<String,String> feedHeaderMap = new HashMap<>();
    private String Url = "http://publisher.sugrqb.in/http://publisher.sugrqb.in/api/?json=get_category_posts&slug=medical_updates";
    private FeedData responseData;
    private ListAdapter feedListAdapter;
    private ListView feedListView;
    private View v;
    private ProgressDialog dialog;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.feed_fragment,container,false);

        try {
            RestClient feedRestClient = RestClient.getInstance(getActivity().getApplicationContext());
            feedHeaderMap.put("Content_type","application/json");
            Call<FeedData> jsonObjectCall = feedRestClient.api().getFeedData(feedHeaderMap,Url);
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage("Loading...");
            dialog.setCancelable(false);
            dialog.show();
            jsonObjectCall.enqueue(new Callback<FeedData>() {
                @Override
                public void onResponse(Call<FeedData> call, Response<FeedData> response) {
                    if(dialog.isShowing()){
                        dialog.dismiss();
                    }
                    responseData = response.body();
                    feedListAdapter = new FeedAdapter(getActivity(),response.body().getPosts());
                    feedListView = (ListView) v.findViewById(R.id.feed_listFragment);
                    feedListView.setAdapter(feedListAdapter);

                    //OnItemClick
                    feedListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                            Intent intent = new Intent(getContext(), FeedWebViewActivity.class);
                            intent.putExtra("URL",responseData.getPosts().get(position).getUrl());
                            intent.putExtra("NAME",responseData.getPosts().get(position).getTitle());
                            startActivity(intent);
                        }
                    });
                }

                @Override
                public void onFailure(Call<FeedData> call, Throwable t) {
                    t.printStackTrace();
                    Toast.makeText(getContext(),"Please try again after sometime",Toast.LENGTH_LONG).show();
                }
            });
        }catch (Exception e)
        {
            e.printStackTrace();
        }
        return v;
    }
}
