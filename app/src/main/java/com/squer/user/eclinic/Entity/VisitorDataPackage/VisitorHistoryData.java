package com.squer.user.eclinic.Entity.VisitorDataPackage;

/**
 * Created by user on 11/23/2017.
 */

public class VisitorHistoryData {

    private HistoryRef reference;
    private HistoryDoctor doctor;
    private HistoryPatient patient;
    private String dateOfVisit;
    private String prevHistory;
    private String remarks;
    private String nextVisitOn;
    private PrescriptionHistory prescription;

    public HistoryRef getReference() {
        return reference;
    }

    public void setReference(HistoryRef reference) {
        this.reference = reference;
    }

    public HistoryDoctor getDoctor() {
        return doctor;
    }

    public void setDoctor(HistoryDoctor doctor) {
        this.doctor = doctor;
    }

    public HistoryPatient getPatient() {
        return patient;
    }

    public void setPatient(HistoryPatient patient) {
        this.patient = patient;
    }

    public String getDateOfVisit() {
        return dateOfVisit;
    }

    public void setDateOfVisit(String dateOfVisit) {
        this.dateOfVisit = dateOfVisit;
    }

    public String getPrevHistory() {
        return prevHistory;
    }

    public void setPrevHistory(String prevHistory) {
        this.prevHistory = prevHistory;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getNextVisitOn() {
        return nextVisitOn;
    }

    public void setNextVisitOn(String nextVisitOn) {
        this.nextVisitOn = nextVisitOn;
    }

    public PrescriptionHistory getPrescription() {
        return prescription;
    }

    public void setPrescription(PrescriptionHistory prescription) {
        this.prescription = prescription;
    }
}
