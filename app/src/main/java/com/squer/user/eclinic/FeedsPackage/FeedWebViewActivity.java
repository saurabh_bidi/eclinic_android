package com.squer.user.eclinic.FeedsPackage;

import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.squer.user.eclinic.R;
import com.squerlib.standard.AbstractAsyncActivity;

public class FeedWebViewActivity extends AbstractAsyncActivity {

    private WebView feedWebView;
    private TextView FeedToolbarText;
    private String Url;
    private String Name;
    private ImageView backButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_web_view);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }
        Url = getIntent().getExtras().getString("URL");
        Name = getIntent().getExtras().getString("NAME");
        FeedToolbarText = (TextView) findViewById(R.id.feed_toolbar_text);
        FeedToolbarText.setText(Name);
        onBackImagePressed();
        getWebView();
    }

    public void getWebView(){
        feedWebView = (WebView) findViewById(R.id.feed_webView);
        feedWebView.getSettings().setJavaScriptEnabled(true);
        feedWebView.loadUrl(Url);
    }

    public void onBackImagePressed(){
        backButton = (ImageView) findViewById(R.id.back_feedWebView) ;
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}