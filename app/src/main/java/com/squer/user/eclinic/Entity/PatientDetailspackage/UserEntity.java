package com.squer.user.eclinic.Entity.PatientDetailspackage;

import com.squerlib.session.UserSessionEntity;

/**
 * Created by user on 10/26/2017.
 */

public class UserEntity implements UserSessionEntity {

    String userId;
    String certificate;
    String name;
    String ownerId;

    @Override
    public String getCertificate() {
        return certificate;
    }

    @Override
    public String getTenantId() {
        return null;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }
}
