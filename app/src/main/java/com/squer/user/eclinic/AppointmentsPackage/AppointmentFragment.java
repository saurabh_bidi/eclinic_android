package com.squer.user.eclinic.AppointmentsPackage;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squer.user.eclinic.Entity.AppointmentsDataPackage.AppointmentsData;
import com.squer.user.eclinic.Entity.AppointmentsDataPackage.PostAppointmentData;
import com.squer.user.eclinic.R;
import com.squer.user.eclinic.RescheduleActivity;
import com.squer.user.eclinic.Rest.RestClient;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.widget.Toast.LENGTH_LONG;

/**
 * Created by user on 11/27/2017.
 */

public class AppointmentFragment extends Fragment {
    private View v;
    private String date;
    private CalendarView calendarView;
    private ListView appointmentList;
    private String doctorId;
    private AppointmentsAdapter appointmentsAdapter;
    private ProgressDialog dialog;
    private static String selectedDate;
    private Random rnd;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.appointment_fragment, container, false);
        doctorId = getArguments().getString("Doctor Id");
        calendarView = (CalendarView) v.findViewById(R.id.calendarView);
        getAppointments();
        getAppointmentsForDate();
        return v;
    }

    public void getAppointments(){
        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading..");
        dialog.setCancelable(true);
        dialog.show();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String formattedDate = dateFormat.format(date);
        RestClient appointmentClient = RestClient.getInstance(getContext());
        if(selectedDate==null){
            Call<List<AppointmentsData>> jsonArrayCall = appointmentClient.api().getAppointmentsForDate(appointmentClient.getHeaderMap(),formattedDate,doctorId);
            jsonArrayCall.enqueue(new Callback<List<AppointmentsData>>() {
                @Override
                public void onResponse(Call<List<AppointmentsData>> call, Response<List<AppointmentsData>> response) {
                    try {
                        if(response.isSuccessful()){
                            if(dialog.isShowing()){
                                dialog.dismiss();
                            }

                            appointmentsAdapter = new AppointmentsAdapter(getContext(), response.body());
                            appointmentList = (ListView) v.findViewById(R.id.Appointments_list);
                            appointmentList.setAdapter(appointmentsAdapter);
                        }else {
                            dialog.setMessage("Failed");
                            dialog.show();
                        }

                    }catch (Exception  e){
                        if(dialog.isShowing()){
                            dialog.dismiss();
                        }
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<List<AppointmentsData>> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }else {

            Call<List<AppointmentsData>> jsonArrayCall = appointmentClient.api().getAppointmentsForDate(appointmentClient.getHeaderMap(),selectedDate,doctorId);
            jsonArrayCall.enqueue(new Callback<List<AppointmentsData>>() {
                @Override
                public void onResponse(Call<List<AppointmentsData>> call, Response<List<AppointmentsData>> response) {
                    try {

                        try {
                            calendarView.setDate(new SimpleDateFormat("yyyy-MM-dd").parse(selectedDate).getTime(), true, true);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        if(response.isSuccessful()){
                            if(dialog.isShowing()){
                                dialog.dismiss();
                            }

                            appointmentsAdapter = new AppointmentsAdapter(getContext(), response.body());
                            appointmentList = (ListView) v.findViewById(R.id.Appointments_list);
                            appointmentList.setAdapter(appointmentsAdapter);
                        }else {
                            dialog.setMessage("Failed");
                            dialog.show();
                        }

                    }catch (Exception  e){
                        if(dialog.isShowing()){
                            dialog.dismiss();
                        }
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<List<AppointmentsData>> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }

    }

    public void getAppointmentsForDate()
    {
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                date = year + "-" + (month + 1) + "-" + dayOfMonth;
                selectedDate = date;

                try {
                    dialog.setMessage("Loading...");
                    dialog.show();
                    RestClient appointmentClient = RestClient.getInstance(getContext());
                    Call<List<AppointmentsData>> jsonArrayCall = appointmentClient.api().getAppointmentsForDate(appointmentClient.getHeaderMap(),date,doctorId);
                    jsonArrayCall.enqueue(new Callback<List<AppointmentsData>>() {
                        @Override
                        public void onResponse(Call<List<AppointmentsData>> call, Response<List<AppointmentsData>> response) {
                            try {
                                if(response.isSuccessful()){
                                    if(dialog.isShowing()){
                                        dialog.dismiss();
                                    }

                                    appointmentsAdapter = new AppointmentsAdapter(getContext(), response.body());
                                    appointmentList = (ListView) v.findViewById(R.id.Appointments_list);
                                    appointmentList.setAdapter(appointmentsAdapter);
                                }

                            }catch (Exception  e){
                                if(dialog.isShowing()){
                                    dialog.dismiss();
                                }
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onFailure(Call<List<AppointmentsData>> call, Throwable t) {
                            if(dialog.isShowing()){
                                dialog.dismiss();
                            }
                            t.printStackTrace();
                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public int getRandomColor(){
        rnd = new Random();
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
    }





    //Adapter
    public class AppointmentsAdapter extends ArrayAdapter <AppointmentsData>{

        private TextView clinicText,acceptText,declineText,statusText,starttime,endtime,profileText,toText,rescheduleText;
        private String confirmed = "SYSLV00000000000000000000000000000010";
        private String rejected = "SYSLV00000000000000000000000000000012";
        private String appointmentId,patientId,clinicId;
        private View listItemView;
        private LinearLayout slotLayout;
        private CircleImageView profileImage,statusImage;

        public AppointmentsAdapter(Context context, List<AppointmentsData> responseData) {
            super(context, R.layout.custom_appointments_list, responseData);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            int[] androidColors = getResources().getIntArray(R.array.androidcolors);
            int randomAndroidColor = androidColors[new Random().nextInt(androidColors.length)];
       try {
           AppointmentsData appointmentsData = getItem(position);
           listItemView = LayoutInflater.from(getContext()).inflate(R.layout.custom_appointments_list,parent,false);

           final AppointmentsData jsonResponse = getItem(position);
           acceptText = (TextView) listItemView.findViewById(R.id.accept_text);
           rescheduleText = (TextView) listItemView.findViewById(R.id.reschedule_text);
           declineText =  (TextView) listItemView.findViewById(R.id.decline_text);
           starttime = (TextView) listItemView.findViewById(R.id.start_time);
           endtime = (TextView) listItemView.findViewById(R.id.end_time);
           profileText = (TextView) listItemView.findViewById(R.id.profile_text);
           profileImage = (CircleImageView) listItemView.findViewById(R.id.profile_image);
           profileImage.setBackgroundColor(getRandomColor());
           statusImage = (CircleImageView) listItemView.findViewById(R.id.status_image);
           statusText = (TextView) listItemView.findViewById(R.id.status_text);
           toText = (TextView) listItemView.findViewById(R.id.to_text);
           appointmentId = jsonResponse.getAppointmentid();
           patientId = jsonResponse.getPatient_id();
           clinicId = jsonResponse.getClinic_id();

           if(jsonResponse.getEmployee_name()==null){

               acceptText.setVisibility(View.INVISIBLE);
               declineText.setVisibility(View.INVISIBLE);
               rescheduleText.setVisibility(View.INVISIBLE);
           }

           if(jsonResponse.getClinic_name()==null){

               acceptText.setVisibility(View.INVISIBLE);
               declineText.setVisibility(View.INVISIBLE);
               rescheduleText.setVisibility(View.INVISIBLE);
           }

           if(jsonResponse.getApp_start_time()!=null){
               TextView startTime = (TextView) listItemView.findViewById(R.id.start_time);
               startTime.setText(jsonResponse.getApp_start_time());
               toText.setVisibility(View.VISIBLE);
           }

           if(jsonResponse.getApp_end_time()!=null){
               TextView endTime = (TextView) listItemView.findViewById(R.id.end_time);
               endTime.setText(jsonResponse.getApp_end_time());
           }

           TextView patientName = (TextView) listItemView.findViewById(R.id.patient_name);
           patientName.setText(jsonResponse.getPatient_name());

           if(jsonResponse.getPatient_name()!=null){

               String nameInitial = jsonResponse.getPatient_name();
               String result = nameInitial.substring(0,1);
               profileText.setVisibility(View.VISIBLE);
               profileText.setText(result);
           }

           TextView clinicName = (TextView) listItemView.findViewById(R.id.clinic_name);
           clinicName.setText(jsonResponse.getClinic_name());

           try {
               if(jsonResponse.getStatus_id().equals("SYSLV00000000000000000000000000000010")){
                   acceptText.setVisibility(View.INVISIBLE);
                   declineText.setVisibility(View.INVISIBLE);
                   statusImage.setVisibility(View.VISIBLE);
                   rescheduleText.setVisibility(View.INVISIBLE);
                   statusImage.setImageResource(R.color.colorGreen);
                   statusText.setVisibility(View.VISIBLE);
               }

               if(jsonResponse.getStatus_id().equals("SYSLV00000000000000000000000000000012")){
                   acceptText.setVisibility(View.INVISIBLE);
                   declineText.setVisibility(View.INVISIBLE);
                   patientName.setText("");
                   clinicName.setText("");
                   profileText.setText("");

               }
           }catch (Exception e){
               e.printStackTrace();
           }

           acceptText.setTag(appointmentId);
           acceptText.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   dialog.setMessage("Please Wait..");
                   dialog.show();
                   RestClient appointmentPostclient = RestClient.getInstance(getContext());
                   PostAppointmentData appointmentData = new PostAppointmentData((String) v.getTag(),confirmed);
                   Call<JSONObject> jsonObjectCall = appointmentPostclient.api().postAppointment(appointmentPostclient.getHeaderMap(),appointmentData);
                   jsonObjectCall.enqueue(new Callback<JSONObject>() {
                       @Override
                       public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                           if(response.isSuccessful()){
                               if(dialog.isShowing()){
                                   dialog.dismiss();
                               }
                               Toast.makeText(getContext(),"Appointment Accepted", LENGTH_LONG).show();
                               FragmentTransaction ft = getFragmentManager().beginTransaction();
                               ft.detach(AppointmentFragment.this).attach(AppointmentFragment.this).commit();
                           }else {
                               if(dialog.isShowing()){
                                   dialog.dismiss();
                               }
                               Toast.makeText(getContext(),"Failed", LENGTH_LONG).show();
                           }
                       }

                       @Override
                       public void onFailure(Call<JSONObject> call, Throwable t) {
                           if(dialog.isShowing()){
                               dialog.dismiss();
                           }
                           t.printStackTrace();
                       }
                   });
               }
           });


           rescheduleText.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   Intent intent = new Intent(getContext(), RescheduleActivity.class);
                   intent.putExtra("Patient Id",jsonResponse.getPatient_id());
                   intent.putExtra("Clinic Id",jsonResponse.getClinic_id());
                   startActivity(intent);
               }
           });

           declineText.setTag(appointmentId);
           declineText.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(final View v) {
                   AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
                   alertDialogBuilder.setMessage("Are you sure you want to decline this appointment? ");
                   alertDialogBuilder.setPositiveButton("Yes",
                   new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface arg0, int arg1) {

                           dialog.setMessage("Please Wait..");
                           dialog.show();
                           RestClient appointmentPostclient = RestClient.getInstance(getContext());
                           PostAppointmentData appointmentData = new PostAppointmentData((String) v.getTag(),rejected);
                           Call<JSONObject> jsonObjectCall = appointmentPostclient.api().postAppointment(appointmentPostclient.getHeaderMap(),appointmentData);
                           jsonObjectCall.enqueue(new Callback<JSONObject>() {
                               @Override
                               public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                                   if(response.isSuccessful()){
                                       if(dialog.isShowing()){
                                           dialog.dismiss();
                                       }
                                       Toast.makeText(getContext(),"Appointment Rejected", LENGTH_LONG).show();
                                       FragmentTransaction ft = getFragmentManager().beginTransaction();
                                       ft.detach(AppointmentFragment.this).attach(AppointmentFragment.this).commit();

                                   }else {
                                       if(dialog.isShowing()){
                                           dialog.dismiss();
                                       }
                                       Toast.makeText(getContext(),"Failed", LENGTH_LONG).show();
                                   }
                               }

                               @Override
                               public void onFailure(Call<JSONObject> call, Throwable t) {
                                   if(dialog.isShowing()){
                                       dialog.dismiss();
                                   }
                                   t.printStackTrace();
                               }
                           });
                       }
                   });

                   alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
                       @Override
                       public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                       }
                   });
                   AlertDialog alertDialog = alertDialogBuilder.create();
                   alertDialog.show();
               }
           });


       }catch (Exception e){
           e.printStackTrace();
       }
            return listItemView;
       }
    }
}
