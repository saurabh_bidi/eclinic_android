package com.squer.user.eclinic.Entity.VisitorDataPackage;

import java.util.List;

/**
 * Created by user on 11/23/2017.
 */

public class PrescriptionHistory {

    private PresRefId reference;
    private Visit visit;
    private String diagnosis;
    private String onExamination;
    private Double weight;
    private Double height;
    private String comments;
    private List<HistoryPresLines> lines;

    public PresRefId getReference() {
        return reference;
    }

    public void setReference(PresRefId reference) {
        this.reference = reference;
    }

    public Visit getVisit() {
        return visit;
    }

    public void setVisit(Visit visit) {
        this.visit = visit;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getOnExamination() {
        return onExamination;
    }

    public void setOnExamination(String onExamination) {
        this.onExamination = onExamination;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public List<HistoryPresLines> getLines() {
        return lines;
    }

    public void setLines(List<HistoryPresLines> lines) {
        this.lines = lines;
    }
}
