package com.squer.user.eclinic.Entity.VisitorDataPackage;

/**
 * Created by user on 11/18/2017.
 */

public class PLRefData {
    private String id;

    public PLRefData(String id){
        this.id=id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
