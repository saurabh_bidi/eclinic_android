package com.squer.user.eclinic.Entity.VisitorDataPackage;

import java.util.Date;

/**
 * Created by user on 11/18/2017.
 */

public class NewVisitorData {
    private DoctorData doctor;
    private PatientData patient;
    private String dateOfVisit;
    private String prevHistory;
    private String remarks;
    private String nextVisitOn;
    private PrescriptionData prescription;

    public NewVisitorData(DoctorData doctor,PatientData patient,String dateOfVisit,
                          String prevHistory,String remarks,String nextVisitOn,PrescriptionData prescription){
        this.doctor=doctor;
        this.patient = patient;
        this.dateOfVisit=dateOfVisit;
        this.prevHistory=prevHistory;
        this.remarks=remarks;
        this.nextVisitOn=nextVisitOn;
        this.prescription=prescription;
    }

    public DoctorData getDoctor() {
        return doctor;
    }

    public void setDoctor(DoctorData doctor) {
        this.doctor = doctor;
    }

    public PatientData getPatient() {
        return patient;
    }

    public void setPatient(PatientData patient) {
        this.patient = patient;
    }

    public String getDateOfVisit() {
        return dateOfVisit;
    }

    public void setDateOfVisit(String dateOfVisit) {
        this.dateOfVisit = dateOfVisit;
    }

    public void setNextVisitOn(String nextVisitOn) {
        this.nextVisitOn = nextVisitOn;
    }

    public String getPrevHistory() {
        return prevHistory;
    }

    public void setPrevHistory(String prevHistory) {
        this.prevHistory = prevHistory;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getNextVisitOn() {
        return nextVisitOn;
    }

    public PrescriptionData getPrescription() {
        return prescription;
    }

    public void setPrescription(PrescriptionData prescription) {
        this.prescription = prescription;
    }
}
