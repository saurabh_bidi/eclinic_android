package com.squer.user.eclinic.Entity.PatientProfilePackage;

/**
 * Created by user on 11/17/2017.
 */

public class AddressRef {

    private String id;

    public AddressRef(String id){
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
