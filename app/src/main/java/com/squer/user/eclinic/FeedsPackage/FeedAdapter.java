package com.squer.user.eclinic.FeedsPackage;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squer.user.eclinic.Entity.FeedDataPackage.FeedPostsData;
import com.squer.user.eclinic.R;

import java.util.List;

/**
 * Created by user on 11/30/2017.
 */

public class FeedAdapter extends ArrayAdapter<FeedPostsData> {

    public FeedAdapter(Context context, List<FeedPostsData> responseData) {
        super(context, R.layout.custom_feed_layout , responseData);

    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        View customView = layoutInflater.inflate(R.layout.custom_feed_layout,parent,false);
        FeedPostsData jsonResponse = getItem(position);
        TextView headingText = (TextView) customView.findViewById(R.id.feed_title_text_view);
        TextView originText = (TextView) customView.findViewById(R.id.feed_origin_text_view);
        TextView descriptionText = (TextView) customView.findViewById(R.id.feed_description_text_view);

        if(jsonResponse.getAttachments() !=null){
            //Set Display Picture
            try {
                ImageView displayImage = (ImageView) customView.findViewById(R.id.feed_icon);
                String imageUrl = jsonResponse.getAttachments().get(position).getImages().getThumbnail().getUrl();
                Picasso.with(getContext()).load(imageUrl).into(displayImage);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

        headingText.setText(jsonResponse.getTitle());
        originText.setText(jsonResponse.getTitle_plain());
        descriptionText.setText(Html.fromHtml(Html.fromHtml(jsonResponse.getExcerpt()).toString()));

        return customView;
    }
}
