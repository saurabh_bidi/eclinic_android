package com.squer.user.eclinic.PatientsPackage;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.parser.Path;
import com.squer.user.eclinic.Entity.VisitorDataPackage.LineDTO;
import com.squer.user.eclinic.Entity.VisitorDataPackage.PrescriptionData;
import com.squer.user.eclinic.Entity.VisitorDataPackage.PrescriptionDtO;
import com.squer.user.eclinic.Entity.VisitorDataPackage.VisitorHistoryData;
import com.squer.user.eclinic.R;
import com.squer.user.eclinic.Rest.RestClient;
import com.squerlib.standard.AbstractAsyncActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrescriptionActivity extends AbstractAsyncActivity {
    private TextView diagnosis,onExam,comments,height,weight,instructions;
    private String prescriptionId;
    private Button print;
    private PrescriptionDtO responseData;
    private PrescriptionAdapter presAdapter;
    private ListView prescriptionList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prescription);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }
        diagnosis = (TextView) findViewById(R.id.diagnosis_text_history);
        onExam = (TextView) findViewById(R.id.onExamination_text_history);
        comments = (TextView) findViewById(R.id.comments_text_history);
        height = (TextView) findViewById(R.id.editText_height_history);
        weight = (TextView) findViewById(R.id.editText_weight_history);
        instructions = (TextView) findViewById(R.id.instructions_text_history);
        print = (Button) findViewById(R.id.textView_print) ;
        prescriptionId = getIntent().getExtras().getString("Prescription Id");
        getPrescriptionDetails();
        onPrintClicked();

    }

    public void getPrescriptionDetails(){

       try {
           showProgressDialog("Loading Prescription...",true);
           final RestClient presRestClient = RestClient.getInstance(getApplicationContext());
           Call<PrescriptionDtO> jsonObjectCall = presRestClient.api().getPrescription(presRestClient.getHeaderMap(),prescriptionId);
           jsonObjectCall.enqueue(new Callback<PrescriptionDtO>() {
               @Override
               public void onResponse(Call<PrescriptionDtO> call, Response<PrescriptionDtO> response) {
                   if(response.isSuccessful()){
                       dismissProgressDialog();
                       responseData = response.body();
                       diagnosis.setText(responseData.getDiagnosis());
                       onExam.setText(responseData.getOnExamination());
                       comments.setText(responseData.getComments());
                       height.setText(responseData.getHeight()+"");
                       weight.setText(responseData.getWeight()+"");

                       presAdapter = new PrescriptionAdapter(getApplicationContext(),responseData.getPrescriptionLineDTO());
                       prescriptionList = (ListView) findViewById(R.id.medicine_list);
                       prescriptionList.setAdapter(presAdapter);
                   }else {
                       dismissProgressDialog();
                   }
               }
               @Override
               public void onFailure(Call<PrescriptionDtO> call, Throwable t) {
                   dismissProgressDialog();
                   t.printStackTrace();
               }
           });
       }catch (Exception e){
           e.printStackTrace();
       }
    }
    //Take Screenshot
    private Bitmap takeScreenshot(View v)
    {
        Bitmap screenshot = null;
        try{
            int width = v.getMeasuredWidth();
            int height = v.getMeasuredHeight();
            screenshot = Bitmap.createBitmap(width,height, Bitmap.Config.ARGB_8888);

            //Draw to Canvas
            Canvas canvas = new Canvas(screenshot);
            v.draw(canvas);

        }catch (Exception e){
            e.printStackTrace();
        }
        return screenshot;
    }


    private void saveScreenshot(Bitmap bitmap){
        ByteArrayOutputStream bao = null;
        File file = null;

        try {
            bao = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG,40,bao);

            //Write as file to Sd card
            file = new File(Environment.getExternalStorageDirectory()+File.separator+"Prescription.png");
            file.createNewFile();

            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bao.toByteArray());
            fos.close();
            if(file!=null){
                Document document = new Document();
                PdfWriter.getInstance(document,new FileOutputStream(Environment.getExternalStorageDirectory()+"/"+GetDate()+"Prescription.pdf"));
                document.open();
                Image image = Image.getInstance(file.toString());
                float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
                  - document.rightMargin() - 0) / image.getWidth()
                ) * 100;
                    image.scalePercent(30);
                image.setAlignment(Image.TEXTWRAP);
                document.add(image);
                document.close();
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    public String GetDate()
    {
        Date date = new Date();
        return date.toString();
    }





    public void onPrintClicked(){
       final LinearLayout linearLayout = (LinearLayout) findViewById(R.id.Prescription_layout_new);

        print.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View v) {
            Bitmap pic = takeScreenshot(linearLayout);

                try {
                    if(pic!=null){
                        saveScreenshot(pic);
                        Toast.makeText(getApplicationContext(),"Screenshot Captured",Toast.LENGTH_LONG).show();
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
             }

    });
}

    //Adapter
    public class PrescriptionAdapter extends ArrayAdapter<LineDTO>{
        public PrescriptionAdapter(Context context, List<LineDTO> responseData) {
            super(context, R.layout.prescription_medicine_list, responseData);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            View listItemView = convertView;
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.prescription_medicine_list,parent,false);

            final LineDTO jsonResponse = getItem(position);
            TextView brandName = (TextView) listItemView.findViewById(R.id.brand_text_list);
            brandName.setText(jsonResponse.getBrandname());

            TextView dosageName = (TextView) listItemView.findViewById(R.id.dosage_text_list);
            dosageName.setText(jsonResponse.getDosage());

            TextView instructions = (TextView) listItemView.findViewById(R.id.instructions_text_history);
            instructions.setText(jsonResponse.getInstructions());

            return listItemView;
        }
    }
}
