package com.squer.user.eclinic.Entity.VisitorDataPackage;

import java.util.List;

/**
 * Created by user on 11/25/2017.
 */

public class PrescriptionDtO {

    private String diagnosis;
    private String onExamination;
    private Double weight;
    private Double height;
    private String comments;
    private List<LineDTO> prescriptionLineDTO;

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getOnExamination() {
        return onExamination;
    }

    public void setOnExamination(String onExamination) {
        this.onExamination = onExamination;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public List<LineDTO> getPrescriptionLineDTO() {
        return prescriptionLineDTO;
    }

    public void setPrescriptionLineDTO(List<LineDTO> prescriptionLineDTO) {
        this.prescriptionLineDTO = prescriptionLineDTO;
    }
}
