package com.squer.user.eclinic.Entity.VisitorDataPackage;

/**
 * Created by user on 11/23/2017.
 */

public class HistoryPresLines {
    private PresRefId prescription;
    private int lineNo;
    private PresLineRef reference;
    private HistoryBrand brand;
    private HistoryDosage dosage;
    private String instructions;

    public PresRefId getPrescription() {
        return prescription;
    }

    public void setPrescription(PresRefId prescription) {
        this.prescription = prescription;
    }

    public int getLineNo() {
        return lineNo;
    }

    public void setLineNo(int lineNo) {
        this.lineNo = lineNo;
    }

    public PresLineRef getReference() {
        return reference;
    }

    public void setReference(PresLineRef reference) {
        this.reference = reference;
    }

    public HistoryBrand getBrand() {
        return brand;
    }

    public void setBrand(HistoryBrand brand) {
        this.brand = brand;
    }

    public HistoryDosage getDosage() {
        return dosage;
    }

    public void setDosage(HistoryDosage dosage) {
        this.dosage = dosage;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }
}
