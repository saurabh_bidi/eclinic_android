package com.squer.user.eclinic;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

import com.squer.user.eclinic.AppointmentsPackage.AppointmentFragment;
import com.squer.user.eclinic.FeedsPackage.FeedFragment;
import com.squer.user.eclinic.PatientsPackage.PatientFragment;
import com.squer.user.eclinic.SettingsPackage.SettingsFragment;

public class HomeActivity extends AppCompatActivity {

    private TextView mTextMessage;
    private String doctorId;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.appointments:
                    AppointmentFragment appointmentFragment = new AppointmentFragment();
                    Bundle bundle1 = new Bundle();
                    bundle1.putString("Doctor Id",doctorId);
                    appointmentFragment.setArguments(bundle1);
                    FragmentManager manager1 = getSupportFragmentManager();
                    manager1.beginTransaction().replace(R.id.content,appointmentFragment,appointmentFragment.getTag()).commit();
                    setTitle("My Appointments");
                    break;
                case R.id.patients:
                    PatientFragment patientFragment = new PatientFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("Doctor Id",doctorId);
                    patientFragment.setArguments(bundle);
                    FragmentManager manager2 = getSupportFragmentManager();
                    manager2.beginTransaction().replace(R.id.content,patientFragment,patientFragment.getTag()).commit();
                    setTitle("Patients");
                    break;
                case R.id.navigation_home:
                    FeedFragment feedFragment = new FeedFragment();
                    FragmentManager manager = getSupportFragmentManager();
                    manager.beginTransaction().replace(R.id.content,feedFragment,feedFragment.getTag()).commit();
                    setTitle("Feeds");
                    break;
                case R.id.navigation_notifications:
                    SettingsFragment settingsFragment = new SettingsFragment();
                    Bundle bundle2 = new Bundle();
                    bundle2.putString("Doctor Id",doctorId);
                    settingsFragment.setArguments(bundle2);
                    FragmentManager manager3 = getSupportFragmentManager();
                    manager3.beginTransaction().replace(R.id.content,settingsFragment,settingsFragment.getTag()).commit();
                    setTitle("Settings");
                    break;
            }
            return true;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        doctorId = getIntent().getExtras().getString("Doctor Id");

        AppointmentFragment appointmentFragment = new AppointmentFragment();
        Bundle bundle1 = new Bundle();
        bundle1.putString("Doctor Id",doctorId);
        appointmentFragment.setArguments(bundle1);
        FragmentManager manager1 = getSupportFragmentManager();
        manager1.beginTransaction().replace(R.id.content,appointmentFragment,appointmentFragment.getTag()).commit();
        setTitle("My Appointments");

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    @Override
    public void onBackPressed() {

    }
}
