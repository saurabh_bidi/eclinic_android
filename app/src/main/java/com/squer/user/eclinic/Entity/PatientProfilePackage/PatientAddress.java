package com.squer.user.eclinic.Entity.PatientProfilePackage;

/**
 * Created by user on 11/17/2017.
 */

public class PatientAddress {

    private AddressRef reference;
    private AddressOwner owner;

    private String line1;
    private String line2;
    private String town;
    private String state;
    private String zipCode;
    private AddressType type;

    public PatientAddress(AddressRef reference,AddressOwner owner,String line1,String line2,String town,String state,String zipCode,AddressType type){
        this.reference = reference;
        this.owner = owner;
        this.line1 = line1;
        this.line2 = line2;
        this.town = town;
        this.state = state;
        this.zipCode = zipCode;
        this.type = type;
    }

    public AddressRef getReference() {
        return reference;
    }

    public void setReference(AddressRef reference) {
        this.reference = reference;
    }

    public AddressOwner getOwner() {
        return owner;
    }

    public void setOwner(AddressOwner owner) {
        this.owner = owner;
    }

    public String getLine1() {
        return line1;
    }

    public void setLine1(String line1) {
        this.line1 = line1;
    }

    public String getLine2() {
        return line2;
    }

    public void setLine2(String line2) {
        this.line2 = line2;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public AddressType getType() {
        return type;
    }

    public void setType(AddressType type) {
        this.type = type;
    }
}
