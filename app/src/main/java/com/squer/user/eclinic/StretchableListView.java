package com.squer.user.eclinic;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.ListView;

/**
 * Created by user on 12/1/2017.
 */

public class StretchableListView extends ListView {
    public StretchableListView(Context context) {
        super(context);
    }

    public StretchableListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StretchableListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void drawableStateChanged() {
        super.drawableStateChanged();
        getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    View lastChild = getChildAt(getChildCount() - 1);
                    if (lastChild != null) {
                        int height = 0;
                        height = lastChild.getBottom();
                        int childHeight = getChildAt(0).getBottom();
                        float child = getAdapter().getCount();
                        height = (int) (child * (childHeight + getDividerHeight())); //rows * getColumnWidth() + (20 * rows - 1);
                        setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, height));
                    }
                }
            }
        });
    }
}
