package com.squer.user.eclinic.Entity.AppointmentsDataPackage;

/**
 * Created by user on 12/1/2017.
 */

public class PostAppointmentData {

    private String appointmentid;
    private String appointmentstatus;

    public PostAppointmentData(String appointmentid,String appointmentstatus){
        this.appointmentid = appointmentid;
        this.appointmentstatus = appointmentstatus;

    }

    public String getAppointmentid() {
        return appointmentid;
    }

    public void setAppointmentid(String appointmentid) {
        this.appointmentid = appointmentid;
    }

    public String getAppointmentstatus() {
        return appointmentstatus;
    }

    public void setAppointmentstatus(String appointmentstatus) {
        this.appointmentstatus = appointmentstatus;
    }
}
