package com.squer.user.eclinic.Entity.PatientDetailspackage;

/**
 * Created by user on 11/16/2017.
 */

public class GenderData {
    private String id;
    private String displayName;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
