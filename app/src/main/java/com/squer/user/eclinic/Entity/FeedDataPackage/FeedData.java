package com.squer.user.eclinic.Entity.FeedDataPackage;

import java.util.List;

/**
 * Created by user on 10/27/2017.
 */

public class FeedData {

    private String status;
    private int count;
    private int pages;

    private FeedCategoryData category;
    private List<FeedPostsData> posts;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public FeedCategoryData getCategory() {
        return category;
    }

    public void setCategory(FeedCategoryData category) {
        this.category = category;
    }

    public List<FeedPostsData> getPosts() {
        return posts;
    }

    public void setPosts(List<FeedPostsData> posts) {
        this.posts = posts;
    }
}
