package com.squer.user.eclinic.Entity.FeedDataPackage;

/**
 * Created by user on 10/27/2017.
 */

public class FeedImagesData {

    private Full_Images full;
    private Thumbnail_Images thumbnail;
    private Medium_Images medium;
    private Large_Images large;
    private PostThumbnail post;


    public Full_Images getFull() {
        return full;
    }

    public void setFull(Full_Images full) {
        this.full = full;
    }

    public Thumbnail_Images getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(Thumbnail_Images thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Medium_Images getMedium() {
        return medium;
    }

    public void setMedium(Medium_Images medium) {
        this.medium = medium;
    }

    public Large_Images getLarge() {
        return large;
    }

    public void setLarge(Large_Images large) {
        this.large = large;
    }


}
