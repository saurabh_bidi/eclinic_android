package com.squer.user.eclinic.PatientsPackage;

import android.app.DatePickerDialog;
import android.app.FragmentTransaction;
import android.graphics.Color;
import java.text.DateFormat;
import android.icu.text.DateFormatSymbols;
import android.os.Build;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import com.squer.user.eclinic.Entity.PatientProfilePackage.AddressOwner;
import com.squer.user.eclinic.Entity.PatientProfilePackage.AddressRef;
import com.squer.user.eclinic.Entity.PatientProfilePackage.PatientProfileData;
import com.squer.user.eclinic.Entity.PatientProfilePackage.PatientUpdateAddress;
import com.squer.user.eclinic.Entity.PatientProfilePackage.ProfReference;
import com.squer.user.eclinic.Entity.PatientProfilePackage.UpdateProfileData;
import com.squer.user.eclinic.R;
import com.squer.user.eclinic.Rest.RestClient;
import com.squerlib.standard.AbstractAsyncActivity;
import java.text.SimpleDateFormat;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Date;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AbstractAsyncActivity {
    private EditText username,contact1,contact2,email,address1,
            address2,dateOfBirth,heightText,weightText,ageText,town,state,zipcode,typeText,firstName,middleName,lastName;
    private ImageView editImg,saveImg,backImg;
    private String dob = "yyyy-MM-dd";
    private int ageInt,AGE;
    private Double weightDouble,heightDouble;
    private RadioButton maleBtn,femaleBtn,marriedBtn,unMarriedBtn,residenceBtn,officeBtn;
    private String patientId,profilePic,typeId,patientCode,monthString;
    private PatientProfileData responseData;
    private ProfReference reference;
    private PatientUpdateAddress addressObject;
    private String genderId;
    private AddressRef addressRef;
    private AddressOwner addressOwner;
    private String maritalStatusId,addressType;
    private Date convertedDate;
    private String parsedDOB;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }
        backImg = (ImageView) findViewById(R.id.myProfile_back);
        firstName = (EditText) findViewById(R.id.profile_firstName);
        middleName = (EditText) findViewById(R.id.profile_middleName);
        lastName = (EditText) findViewById(R.id.profile_lastName);
        username=(EditText) findViewById(R.id.profile_username);
        contact1 = (EditText) findViewById(R.id.profile_contactNumber);
        contact2 = (EditText) findViewById(R.id.profile_contactNumber2);
        email = (EditText) findViewById(R.id.profile_email_id);
        address1=(EditText) findViewById(R.id.profile_address);
        address2 = (EditText) findViewById(R.id.profile_address2);
        saveImg=(ImageView) findViewById(R.id.Profile_save);
        dateOfBirth = (EditText) findViewById(R.id.profile_dob);
        maleBtn = (RadioButton) findViewById(R.id.radio_male);
        femaleBtn = (RadioButton) findViewById(R.id.radio_female);
        patientId = getIntent().getExtras().getString("Patient Id");
        patientCode = getIntent().getExtras().getString("Patient Code");
        heightText = (EditText) findViewById(R.id.profile_height);
        weightText = (EditText) findViewById(R.id.profile_weight);
        ageText = (EditText) findViewById(R.id.profile_age);
        marriedBtn = (RadioButton) findViewById(R.id.radio_married);
        unMarriedBtn = (RadioButton) findViewById(R.id.radio_unmarried);
        town = (EditText) findViewById(R.id.profile_town);
        state = (EditText) findViewById(R.id.profile_state);
        zipcode = (EditText) findViewById(R.id.profile_zipcode);
        residenceBtn = (RadioButton) findViewById(R.id.radio_residence);
        officeBtn = (RadioButton) findViewById(R.id.radio_office);
        onBackImgPressed();
        getProfile();
        onDOBSelected();
        onSavePressed();
    }

    private String getAge(int year, int month, int day){
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
            age--;
        }

        Integer ageInt = new Integer(age);
        String ageS = ageInt.toString();

        return ageS;
    }

    public void onBackImgPressed(){
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void getProfile(){
       try {
           showProgressDialog("Loading Profile...",false);
           RestClient profileRestClient = RestClient.getInstance(getApplicationContext());
           final Call<PatientProfileData> jsonObjectCall = profileRestClient.api().getProfile(profileRestClient.getHeaderMap(),patientId);
           jsonObjectCall.enqueue(new Callback<PatientProfileData>() {
               @Override
               public void onResponse(Call<PatientProfileData> call, Response<PatientProfileData> response) {
                   dismissProgressDialog();
                   responseData = response.body();
                   firstName.setText(responseData.getFirstName());
                   middleName.setText(responseData.getMiddleName());
                   lastName.setText(responseData.getLastName());
                   username.setText(responseData.getName());
                   contact1.setText(responseData.getContactNo1());
                   contact2.setText(responseData.getContactNo2());
                   email.setText(responseData.getEmail());
                   //dateOfBirth.setText(responseData.getDateOfBirth());

                   try{
                       //Format Date
                       final java.text.SimpleDateFormat inputFormat = new java.text.SimpleDateFormat("yyyy-MM-dd");
                       java.text.SimpleDateFormat outputFormat = new java.text.SimpleDateFormat("dd MMMM yyyy");
                       String dateOfVisit = null;
                       if (responseData != null) {
                           dateOfVisit = responseData.getDateOfBirth();
                       }
                       try {
                           Date date = inputFormat.parse(dateOfVisit);
                           String  parsedDate = outputFormat.format(date);
                           dateOfBirth.setText(parsedDate);
                       } catch (ParseException e) {
                           e.printStackTrace();
                       }
                   }catch (Exception e){
                       e.printStackTrace();
                   }

                   try{
                       if(responseData.getGender().getId().equals("USRLVGN000000000000000000000000000002")){
                           maleBtn.setChecked(true);
                       }else {
                           femaleBtn.setChecked(true);
                       }
                   }catch (Exception e){
                       e.printStackTrace();
                   }

                   heightText.setText(responseData.getHeight()+"");
                   weightText.setText(responseData.getWeight()+"");
                   ageText.setText(responseData.getAge()+"");

                   try {
                       if(responseData.getMaritalStatus().getId().equals("USRLVMS000000000000000000000000000002")){
                           marriedBtn.setChecked(true);
                       }else {
                           unMarriedBtn.setChecked(true);
                       }
                   }catch (Exception e){
                       e.printStackTrace();
                   }

                    try {
                        if(responseData.getAddress().getType().getId().equals("USRLVAT000000000000000000000000000001")){
                            residenceBtn.setChecked(true);
                        }else {
                            officeBtn.setChecked(true);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                   town.setText(responseData.getAddress().getTown());
                   state.setText(responseData.getAddress().getState());
                   zipcode.setText(responseData.getAddress().getZipCode());
                   address1.setText(responseData.getAddress().getLine1());
                   address2.setText(responseData.getAddress().getLine2());
               }

               @Override
               public void onFailure(Call<PatientProfileData> call, Throwable t) {
                   dismissProgressDialog();
                   toast("Please try again after sometime");
                   t.printStackTrace();
               }
           });
       }catch (Exception e){
           dismissProgressDialog();
           e.printStackTrace();
       }
    }

    public void getReferenceId(){
        reference=new ProfReference(responseData.getReference().getId());
    }

    public void onDOBSelected(){
        dateOfBirth.setInputType(InputType.TYPE_NULL);
        dateOfBirth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar calendar = Calendar.getInstance();
                int yy = calendar.get(Calendar.YEAR);
                int mm = calendar.get(Calendar.MONTH);
                int dd = calendar.get(Calendar.DAY_OF_MONTH);
                calendar.set(yy,mm,dd);
                final DatePickerDialog datePicker = new DatePickerDialog(EditProfileActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        String date = dayOfMonth+"-"+(monthOfYear+1)+"-"+year;
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        try {
                            Date date1 = sdf.parse(date);
                            SimpleDateFormat sdf2 = new SimpleDateFormat("dd MMMM yyyy");
                            String convertedDate = sdf2.format(date1);
                            dateOfBirth.setText(convertedDate);

                           Date today = new Date();
                           int currentYear = today.getYear();
                           int enteredYear = date1.getYear();

                           String age = String.valueOf((currentYear - enteredYear));
                           ageText.setText(age);


                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }
                }, yy, mm, dd);
                datePicker.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePicker.show();
            }
        });
    }

    public void EnterLine1Address(){
        try {
            addressRef = new AddressRef(responseData.getAddress().getReference().getId());
            addressOwner = new AddressOwner(responseData.getAddress().getOwner().getId());
            addressObject = new PatientUpdateAddress(addressRef,addressOwner,address1.getText().toString(),address2.getText().toString(),
                    town.getText().toString(),state.getText().toString(),zipcode.getText().toString(),typeId);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void EnterLine2Address(){
       try {
           addressRef = new AddressRef(responseData.getAddress().getReference().getId());
           addressOwner = new AddressOwner(responseData.getAddress().getOwner().getId());
           addressObject = new PatientUpdateAddress(addressRef,addressOwner,address1.getText().toString(),address2.getText().toString(),
                   town.getText().toString(),state.getText().toString(),zipcode.getText().toString(),typeId);
       }catch (Exception e){
           e.printStackTrace();
       }
    }

    public void textToDate()  {
        try {
            java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("dd MMMM yyyy");
            java.text.SimpleDateFormat sdf1 = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            convertedDate = sdf.parse(dateOfBirth.getText().toString());
            parsedDOB = sdf1.format(convertedDate);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void textToInteger() {
        try {
            ageInt = Integer.parseInt(ageText.getText().toString());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void weightToDouble(){
        try {
            weightDouble = Double.parseDouble(weightText.getText().toString());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void heightToDouble(){
        try {
            heightDouble = Double.parseDouble(heightText.getText().toString());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void onSavePressed(){
        saveImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               try {
                   if(TextUtils.isEmpty(username.getText())){
                       toast("Username cannot be empty");
                       return;
                   }
                   if(TextUtils.isEmpty(firstName.getText())){
                       toast("First Name cannot be empty");
                       return;
                   }
                   if(TextUtils.isEmpty(lastName.getText())){
                       toast("Last Name cannot be empty");
                       return;
                   }
                   if(TextUtils.isEmpty(email.getText())){
                       toast("Email cannot be empty");
                       return;
                   }
                   if(!email.getText().toString().matches("[a-zA-Z0-9._-]+@[a-z]+.[a-z]+")){
                        toast("Invalid Email");
                        return;
                   }
                   if(TextUtils.isEmpty(ageText.getText())){
                       toast("Age cannot be empty");
                       return;
                   }

                   if(TextUtils.isEmpty(dateOfBirth.getText())){
                       toast("Date of Birth cannot be empty");
                       return;
                   }

                   showProgressDialog("Updating..",true);
                   if(marriedBtn.isChecked()){
                       maritalStatusId = "USRLVMS000000000000000000000000000002";
                   }else if(unMarriedBtn.isChecked()){
                       maritalStatusId = "USRLVMS000000000000000000000000000001";
                   }
                   if(maleBtn.isChecked()){
                       genderId = "USRLVGN000000000000000000000000000002";
                   }else if(femaleBtn.isChecked()){
                       genderId = "USRLVGN000000000000000000000000000001";
                   }
                   if(residenceBtn.isChecked()){
                       typeId = "USRLVAT000000000000000000000000000001";
                   }else if(officeBtn.isChecked()){
                       typeId = "USRLVGN000000000000000000000000000002";
                   }

                   getReferenceId();
                   EnterLine1Address();
                   EnterLine2Address();
                   textToDate();
                   weightToDouble();
                   heightToDouble();
                   textToInteger();

                   RestClient updateProfileClient = RestClient.getInstance(getApplicationContext());
                   UpdateProfileData profileData = new UpdateProfileData(reference,username.getText().toString(),profilePic,
                           contact1.getText().toString(),contact2.getText().toString(),email.getText().toString(),addressObject,
                           parsedDOB,ageInt,weightDouble,heightDouble,genderId,maritalStatusId,firstName.getText().toString(),
                           middleName.getText().toString(),lastName.getText().toString());
                   Call<JSONObject> jsonObjectCall = updateProfileClient.api().updateProfile(updateProfileClient.getHeaderMap(),
                           profileData);
                   jsonObjectCall.enqueue(new Callback<JSONObject>() {
                       @Override
                       public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                           if(response.isSuccessful()){
                               dismissProgressDialog();
                               toast("Success");
                               finish();
                           }else {
                               dismissProgressDialog();
                               toast("Failed");
                           }
                       }

                       @Override
                       public void onFailure(Call<JSONObject> call, Throwable t) {
                           dismissProgressDialog();
                           t.printStackTrace();
                       }
                   });
               }catch (Exception e){
                   e.printStackTrace();
               }
            }
        });
    }
}
