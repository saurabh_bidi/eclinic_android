package com.squer.user.eclinic.PatientsPackage;

import android.app.DatePickerDialog;
import android.app.FragmentTransaction;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.InputType;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squer.user.eclinic.Entity.VisitorDataPackage.BrandData;
import com.squer.user.eclinic.Entity.VisitorDataPackage.BrandReference;
import com.squer.user.eclinic.Entity.VisitorDataPackage.DoctorData;
import com.squer.user.eclinic.Entity.VisitorDataPackage.DosageData;
import com.squer.user.eclinic.Entity.VisitorDataPackage.DosageReference;
import com.squer.user.eclinic.Entity.VisitorDataPackage.NewVisitorData;
import com.squer.user.eclinic.Entity.VisitorDataPackage.PatientData;
import com.squer.user.eclinic.Entity.VisitorDataPackage.PrescriptionData;
import com.squer.user.eclinic.Entity.VisitorDataPackage.PrescriptionLine;
import com.squer.user.eclinic.R;
import com.squer.user.eclinic.Rest.RestClient;
import com.squerlib.standard.AbstractAsyncActivity;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VisitorActivity extends AbstractAsyncActivity implements View.OnFocusChangeListener {
    private Button addBtn;
    private ImageView saveImg,backImg;
    private Double weightDouble,heightDouble;
    private String formattedDate,doctorId,patientId,inst;
    private EditText visitdateText,nextVisitText,prevHistory,remarks,diagnosis,onExamination,height,weight,comments;
    private AutoCompleteTextView instructions;
    private LinearLayout linearLayout;
    private DoctorData doctorData;
    private PatientData patientData;
    private Date convertedVisitDate,convertedNextVisitDate;
    private PrescriptionData prescriptionData;
    private String parsedVisitDate,parsedNextDate;
    private List<DosageData> dosageResponse;
    private BrandReference brandReference;
    private DosageReference dosageReference;
    private List<String> nameArray = new ArrayList<String>();
    private List<String> dosageNameArray = new ArrayList<String>();
    private ArrayAdapter<String> adapter;
    private ArrayAdapter<String > dosageAdapter;
    private DosageDetails dosageDetails;
    private ArrayList<PrescriptionLine> medicineTextList = new ArrayList<>();
    private HashMap<String,String> map = new HashMap<>();
    private HashMap<String,String > dosageMap = new HashMap<>();

    @Override
    protected void onStart() {
        super.onStart();
        getBrands();
        getDosage();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitor);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

                visitdateText = (EditText) findViewById(R.id.date_text);
                nextVisitText = (EditText) findViewById(R.id.next_visit_date);
                prevHistory = (EditText) findViewById(R.id.prevhis_text);
                remarks = (EditText) findViewById(R.id.remarks_text);
                diagnosis = (EditText) findViewById(R.id.diagnosis_text);
                onExamination = (EditText) findViewById(R.id.onExamination_text);
                height = (EditText) findViewById(R.id.editText_height);
                weight = (EditText) findViewById(R.id.editText_weight);
                comments = (EditText) findViewById(R.id.comments_text);
                doctorId = getIntent().getExtras().getString("Doctor Id");
                patientId = getIntent().getExtras().getString("Patient Id");
                addBtn = (Button) findViewById(R.id.add_btn);
                backImg = (ImageView) findViewById(R.id.imageView_back);
                instructions = (AutoCompleteTextView) findViewById(R.id.instructions_text_inflated);
                linearLayout = (LinearLayout) findViewById(R.id.layout_medicine_container);

        getCurrentDate();
        onVisitDateClicked();
        onNextVisitClicked();
        onAddBtnClicked();
        onAddField();
        addVisitor();
        onBackImgPressed();
    }

    public void onBackImgPressed(){
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void getCurrentDate(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd MMMM yyyy");
        Date date = new Date();
        formattedDate = dateFormat.format(date);
        visitdateText.setText(formattedDate);
    }

    public void onVisitDateClicked(){

        visitdateText.setInputType(InputType.TYPE_NULL);

        visitdateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DateDialog dateDialog = new DateDialog(v);
//                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//                dateDialog.show(fragmentTransaction,"Date Picker");

                final Calendar calendar = Calendar.getInstance();
                int yy = calendar.get(Calendar.YEAR);
                int mm = calendar.get(Calendar.MONTH);
                int dd = calendar.get(Calendar.DAY_OF_MONTH);
                calendar.set(yy,mm,dd);
                final DatePickerDialog datePicker = new DatePickerDialog(VisitorActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        String date = dayOfMonth+"-"+(monthOfYear+1)+"-"+year;
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        try {
                            Date date1 = sdf.parse(date);
                            SimpleDateFormat sdf2 = new SimpleDateFormat("dd MMMM yyyy");
                            String convertedDate = sdf2.format(date1);
                            visitdateText.setText(convertedDate);

//                            Date today = new Date();
//                            int currentYear = today.getYear();
//                            int enteredYear = date1.getYear();
//
//                            String age = String.valueOf((currentYear - enteredYear));
//                            ageText.setText(age);


                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }
                }, yy, mm, dd);
                datePicker.getDatePicker().setMinDate(System.currentTimeMillis());
                datePicker.show();
            }
        });
    }

    public void onNextVisitClicked(){
        nextVisitText.setInputType(InputType.TYPE_NULL);
        nextVisitText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                DateDialog dateDialog = new DateDialog(v);
//                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
//                dateDialog.show(fragmentTransaction,"Date Picker");
                final Calendar calendar = Calendar.getInstance();
                int yy = calendar.get(Calendar.YEAR);
                int mm = calendar.get(Calendar.MONTH);
                int dd = calendar.get(Calendar.DAY_OF_MONTH);
                calendar.set(yy,mm,dd);
                final DatePickerDialog datePicker = new DatePickerDialog(VisitorActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                        String date = dayOfMonth+"-"+(monthOfYear+1)+"-"+year;
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                        try {
                            Date date1 = sdf.parse(date);
                            SimpleDateFormat sdf2 = new SimpleDateFormat("dd MMMM yyyy");
                            String convertedDate = sdf2.format(date1);
                            nextVisitText.setText(convertedDate);

//                            Date today = new Date();
//                            int currentYear = today.getYear();
//                            int enteredYear = date1.getYear();
//
//                            String age = String.valueOf((currentYear - enteredYear));
//                            ageText.setText(age);


                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    }
                }, yy, mm, dd);
                datePicker.getDatePicker().setMinDate(System.currentTimeMillis());
                datePicker.show();
            }
        });


    }

    int prescriptionCount = 0;
    public void onAddField(){
        prescriptionCount++;
        LayoutInflater inflater = LayoutInflater.from(this);
        View rowView = inflater.inflate(R.layout.dosage_list, null);

        // Add the new row before the add field button.

        linearLayout.addView(rowView);
        ImageView removeImg = (ImageView) rowView.findViewById(R.id.remove_view);
        removeImg.setTag(rowView);

        //brand
        AutoCompleteTextView brandText = (AutoCompleteTextView) rowView.findViewById(R.id.brand_text_inflated);
        if(adapter == null)
        adapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.brand_custom_list,nameArray);

        brandText.setAdapter(adapter);
        instructions = rowView.findViewById(R.id.instructions_text_inflated);
        dosageDetails = new DosageDetails(prescriptionCount);
        selectedDosageList.add(dosageDetails);
        brandText.setTag(dosageDetails);
        instructions.setTag(dosageDetails);

        //dosage
        if(dosageAdapter == null)
        dosageAdapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.dosage_custom_list,dosageNameArray);
        AutoCompleteTextView dosageText = (AutoCompleteTextView) rowView.findViewById(R.id.dosage_text_inflated);
        dosageText.setAdapter(dosageAdapter);
        dosageText.setTag(dosageDetails);

        brandText.setOnFocusChangeListener(VisitorActivity.this);
        dosageText.setOnFocusChangeListener(VisitorActivity.this);
        instructions.setOnFocusChangeListener(VisitorActivity.this);
    }

    public void onAddBtnClicked(){
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAddField();
            }
        });
    }

    public Double heightToDouble(EditText et){
        try {
            return Double.parseDouble(et.getText().toString());
        }catch (Exception e){
            e.printStackTrace();
            return 0D;
        }
    }

    public void getDoctorObject(){
        doctorData = new DoctorData(doctorId);
    }

    public void getPatientObject(){
        patientData = new PatientData(patientId);
    }

    public void textToDate(){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            convertedVisitDate = sdf.parse(visitdateText.getText().toString());
            parsedVisitDate = sdf1.format(convertedVisitDate);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void nextTexttoDate(){
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy");
            SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            convertedNextVisitDate = sdf.parse(nextVisitText.getText().toString());
            parsedNextDate = sdf2.format(convertedNextVisitDate);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void getBrands(){
        try {
            final RestClient brandRestClient = RestClient.getInstance(getApplicationContext());
            Call<List<BrandData>> jsonArrayCall = brandRestClient.api().getBrands(brandRestClient.getHeaderMap(), "");
            jsonArrayCall.enqueue(new Callback<List<BrandData>>() {
                @Override
                public void onResponse(Call<List<BrandData>> call, Response<List<BrandData>> response) {

                    if(response.body()!=null){

                        for(BrandData data:response.body()){
                            nameArray.add(data.getBrandName());
                            map.put(data.getBrandName(), data.getBrandid());
                        }
                            adapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.brand_custom_list,nameArray);
//                            brandText = (AutoCompleteTextView) findViewById(R.id.brand_text_inflated);
//                            brandText.setAdapter(adapter);
//                            brandText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                                @Override
//                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                                    String str = adapter.getItem(position);
//                                    brandId = map.get(str);
//
//                                }
//                            });
                    }else {
                        toast("No Brands Found");
                    }
                }

                @Override
                public void onFailure(Call<List<BrandData>> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void getDosage(){
        try {
            final RestClient dosageRestClient = RestClient.getInstance(getApplicationContext());
            Call<List<DosageData>> jsonArrayCall = dosageRestClient.api().getDosage(dosageRestClient.getHeaderMap(), "");
            jsonArrayCall.enqueue(new Callback<List<DosageData>>() {
                @Override
                public void onResponse(Call<List<DosageData>> call, Response<List<DosageData>> response) {

                    if(response.body()!=null){

                        for(DosageData data:response.body()){
                            dosageNameArray.add(data.getDosageName());
                            dosageMap.put(data.getDosageName(), data.getDosageid());
                        }
                        dosageAdapter = new ArrayAdapter<String>(getApplicationContext(),R.layout.dosage_custom_list,dosageNameArray);
//                        dosageText = (AutoCompleteTextView) findViewById(R.id.dosage_text_inflated);
//                        dosageText.setAdapter(dosageAdapter);
//                        dosageText.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                            @Override
//                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                                String str = dosageAdapter.getItem(position);
//                                dosageId = dosageMap.get(str);
//                            }
//                        });
                    }else {
                        toast("No Dosages Found");
                    }

                }

                @Override
                public void onFailure(Call<List<DosageData>> call, Throwable t) {
                    t.printStackTrace();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void addVisitor(){
        saveImg=(ImageView) findViewById(R.id.imageView_save);
        saveImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
           try {
               weightDouble = heightToDouble(weight);
               heightDouble = heightToDouble(height);
               DosageDetails dosageDetails = (DosageDetails) lastFocusedItem.getTag();
               AutoCompleteTextView tv = (AutoCompleteTextView) lastFocusedItem;
               switch (tv.getId()) {
                   case R.id.brand_text:
                   {
                       if(selectedDosageList.contains(dosageDetails))
                       {
                           dosageDetails.brandText = tv.getText().toString().trim();
                       }
                       else
                       {
                           dosageDetails.brandText = tv.getText().toString().trim();
                           selectedDosageList.add(dosageDetails);
                       }
                   }
                   break;
                   case R.id.dosage_text:{
                       if(selectedDosageList.contains(dosageDetails))
                       {
                           dosageDetails.dosageText = tv.getText().toString().trim();
                       }
                       else
                       {
                           dosageDetails.dosageText = tv.getText().toString().trim();
                           selectedDosageList.add(dosageDetails);
                       }
                   }
                   break;
                   case R.id.instructions_text_inflated:{
                       if(selectedDosageList.contains(dosageDetails))
                       {
                           dosageDetails.instructionText = tv.getText().toString().trim();
                       }
                       else
                       {
                           dosageDetails.instructionText = tv.getText().toString().trim();
                           selectedDosageList.add(dosageDetails);
                       }
                   }
                   break;
               }

               Collections.sort(selectedDosageList);
               medicineTextList.clear();
               for (DosageDetails dosageDetails1 : selectedDosageList)
               {
                   brandReference = new BrandReference(map.get(dosageDetails1.brandText));
                   dosageReference = new DosageReference(dosageMap.get(dosageDetails1.dosageText));
                   inst = dosageDetails1.instructionText;

                   PrescriptionLine lineData = new PrescriptionLine(brandReference,dosageReference,inst);
                   medicineTextList.add(lineData);
               }
               prescriptionData = new PrescriptionData(diagnosis.getText().toString(),
                       onExamination.getText().toString(),weightDouble,heightDouble,comments.getText().toString(),
                       medicineTextList );
           }catch (Exception e){
               e.printStackTrace();
           }

                if(TextUtils.isEmpty(diagnosis.getText())){
                    toast("Diagnosis cannot be empty.");
                    return;
                }
                if(TextUtils.isEmpty(onExamination.getText())){
                    toast("On Examination cannot be empty.");
                    return;
                }
                if(TextUtils.isEmpty(visitdateText.getText())){
                    toast("Visit date cannot be empty.");
                    return;
                }
                if(TextUtils.isEmpty(height.getText())){
                    toast("Height cannot be empty.");
                    return;
                }
                if(TextUtils.isEmpty(weight.getText())){
                    toast("Weight cannot be empty.");
                    return;
                }
                if(TextUtils.isEmpty(inst)){
                    toast("Instructions cannot be empty.");
                    return;
                }
                try {

                    getDoctorObject();
                    getPatientObject();
                    textToDate();
                    nextTexttoDate();
                    showProgressDialog("Saving...",true);
                    RestClient visitorRestClient = RestClient.getInstance(getApplicationContext());
                    NewVisitorData visitorData = new NewVisitorData(doctorData,patientData,parsedVisitDate,
                            prevHistory.getText().toString(), remarks.getText().toString(),parsedNextDate,
                            prescriptionData);
                    Call<JSONObject> jsonObjectCall = visitorRestClient.api().addVisit(visitorRestClient.getHeaderMap(),
                            visitorData);
                    jsonObjectCall.enqueue(new Callback<JSONObject>() {
                        @Override
                        public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                            if(response.isSuccessful()){
                                dismissProgressDialog();
                                toast("Successfully Saved");
                                finish();

                            }else {
                                dismissProgressDialog();
                                toast("Failed");
                            }
                        }

                        @Override
                        public void onFailure(Call<JSONObject> call, Throwable t) {
                            dismissProgressDialog();
                            t.printStackTrace();
                        }
                    });
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
    }

    private View lastFocusedItem;
    private List<DosageDetails> selectedDosageList = new ArrayList<>();

    class DosageDetails implements Comparable {
        int index;
        String brandText;
        String dosageText;
        String instructionText;

        public DosageDetails(int prescriptionCount) {
            index = prescriptionCount;
        }

        @Override
        public boolean equals(Object obj) {
            return index == ((DosageDetails)obj).index;
        }

        @Override
        public int compareTo(@NonNull Object o) {
            return index - ((DosageDetails)o).index;
        }
    }
    public void onDelete(View v){
        linearLayout.removeView((View) v.getTag());
        selectedDosageList.remove(dosageDetails);
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {

        if(view instanceof AutoCompleteTextView && view.getTag() instanceof DosageDetails){
            if(hasFocus)
            {
                lastFocusedItem = view;
                return;
            }
            else
            {
                AutoCompleteTextView tv = (AutoCompleteTextView) view;
                dosageDetails = (DosageDetails) tv.getTag();
                switch (tv.getId()) {
                    case R.id.brand_text_inflated:
                            {
                                if(selectedDosageList.contains(dosageDetails))
                                {
                                    dosageDetails.brandText = tv.getText().toString().trim();
                                }
                                    else
                                {
                                    dosageDetails.brandText = tv.getText().toString().trim();
                                    selectedDosageList.add(dosageDetails);
                                }
                            }
                        break;
                    case R.id.dosage_text_inflated:{
                                    if(selectedDosageList.contains(dosageDetails))
                                    {
                                        dosageDetails.dosageText = tv.getText().toString().trim();
                                    }
                                    else
                                    {
                                        dosageDetails.dosageText = tv.getText().toString().trim();
                                        selectedDosageList.add(dosageDetails);
                                    }
                                }
                        break;
                    case R.id.instructions_text_inflated:{
                                    if(selectedDosageList.contains(dosageDetails))
                                    {
                                        dosageDetails.instructionText = tv.getText().toString().trim();
                                    }
                                    else
                                    {
                                        dosageDetails.instructionText = tv.getText().toString().trim();
                                        selectedDosageList.add(dosageDetails);
                                    }
                                }
                        break;
                }
            }
        }



    }

}
