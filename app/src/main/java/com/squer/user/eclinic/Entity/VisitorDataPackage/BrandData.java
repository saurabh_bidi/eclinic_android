package com.squer.user.eclinic.Entity.VisitorDataPackage;

/**
 * Created by user on 11/18/2017.
 */

public class BrandData {
    private String brandid;
    private String brandName;


    public BrandData(String  brandid,String brandName){
        this.brandid=brandid;
        this.brandName=brandName;

    }

    public String getBrandid() {
        return brandid;
    }

    public void setBrandid(String brandid) {
        this.brandid = brandid;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
}
