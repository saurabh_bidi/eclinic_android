package com.squer.user.eclinic.Entity.VisitorDataPackage;

/**
 * Created by user on 11/18/2017.
 */

public class DosageData {

    private String dosageid;
    private String dosageName;


    public DosageData(String dosageid, String dosageName) {
        this.dosageid = dosageid;
        this.dosageName = dosageName;
    }

    public String getDosageid() {
        return dosageid;
    }

    public void setDosageid(String dosageid) {
        this.dosageid = dosageid;
    }

    public String getDosageName() {
        return dosageName;
    }

    public void setDosageName(String dosageName) {
        this.dosageName = dosageName;
    }
}
