package com.squer.user.eclinic.PatientsPackage;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.icu.text.DateFormatSymbols;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by user on 11/15/2017.
 */

public class DateDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private EditText dateText;

    public DateDialog(View v){
        dateText = (EditText)v;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        java.util.Calendar calendar = java.util.Calendar.getInstance();
        int year = calendar.get(java.util.Calendar.YEAR);
        int month = calendar.get(java.util.Calendar.MONTH);
        int day = calendar.get(java.util.Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(),this,year,month,day);
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        //String monthString = new DateFormatSymbols().getMonths()[month];
        String date = day+"-"+(month+1)+"-"+year;
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            Date date1 = sdf.parse(date);
            SimpleDateFormat sdf2 = new SimpleDateFormat("dd MMMM yyyy");
            String convertedDate = sdf2.format(date1);
            dateText.setText(convertedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }


}
