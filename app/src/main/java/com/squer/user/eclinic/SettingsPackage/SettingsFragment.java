package com.squer.user.eclinic.SettingsPackage;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.squer.user.eclinic.Entity.EmployeeDataPackage.EmployeeData;
import com.squer.user.eclinic.LoginActivity;
import com.squer.user.eclinic.R;
import com.squer.user.eclinic.Rest.RestClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user on 11/29/2017.
 */

public class SettingsFragment extends Fragment {
    private View v;
    private String doctorId;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.settings_fragment,container,false);
        doctorId = getArguments().getString("Doctor Id");
        getEmpProfile();
        onLogoutClicked();
        return v;

    }

    public void getEmpProfile(){
        RestClient empRestClient = RestClient.getInstance(getContext());
        Call<EmployeeData> jsonObjectCall = empRestClient.api().getEmpProfile(empRestClient.getHeaderMap(),doctorId);
        jsonObjectCall.enqueue(new Callback<EmployeeData>() {
            @Override
            public void onResponse(Call<EmployeeData> call, Response<EmployeeData> response) {
                EmployeeData responseData = response.body();
                TextView usernameText = (TextView) v.findViewById(R.id.username_profile);
                usernameText.setText(responseData.getName());

                TextView contact = (TextView) v.findViewById(R.id.contactNumber);
                contact.setText(responseData.getMobile());


                View myProfile = v.findViewById(R.id.view_profile);
                myProfile.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), EmployeeProfileActivity.class);
                        intent.putExtra("Doctor Id",doctorId);
                        startActivity(intent);
                    }
                });
            }

            @Override
            public void onFailure(Call<EmployeeData> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    public void onLogoutClicked(){
        Button button = (Button) v.findViewById(R.id.logoutbutton);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), LoginActivity.class);
                startActivity(intent);
            }
        });
    }
}
