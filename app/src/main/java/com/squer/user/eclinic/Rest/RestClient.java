package com.squer.user.eclinic.Rest;

import android.content.Context;

import com.squer.user.eclinic.Entity.AppointmentsDataPackage.AppointmentsData;
import com.squer.user.eclinic.Entity.EmployeeDataPackage.EmployeeData;
import com.squer.user.eclinic.Entity.FeedDataPackage.FeedData;
import com.squer.user.eclinic.Entity.PatientDetailspackage.PatientsData;
import com.squer.user.eclinic.Entity.PatientDetailspackage.UserEntity;
import com.squer.user.eclinic.Entity.PatientProfilePackage.PatientProfileData;
import com.squer.user.eclinic.Entity.PatientProfilePackage.UpdateProfileData;
import com.squer.user.eclinic.Entity.VisitorDataPackage.BrandData;
import com.squer.user.eclinic.Entity.VisitorDataPackage.DosageData;
import com.squer.user.eclinic.Entity.VisitorDataPackage.NewVisitorData;
import com.squer.user.eclinic.Entity.VisitorDataPackage.PrescriptionDtO;
import com.squer.user.eclinic.Entity.VisitorDataPackage.VisitorHistoryData;
import com.squer.user.eclinic.Entity.AppointmentsDataPackage.PostAppointmentData;
import com.squerlib.rest.AbstractRestClient;
import com.squerlib.rest.LoginRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by user on 11/16/2017.
 */

public class RestClient extends AbstractRestClient {

    static RestClient restClient = null;

    protected RestClient(Context context) {
        super(context);
    }

    public static RestClient getInstance(Context ApplicationContext) {
        if(restClient == null) restClient = new RestClient(ApplicationContext);
        return restClient;
    }
    public HashMap<String,String> headers = new HashMap<>();

    public interface RestApiService{

        @POST("login")
        Call<UserEntity> requestLogin(@Body LoginRequest request);

        @GET
        Call<FeedData> getFeedData(@HeaderMap HashMap<String,String> header, @Url String url);

        @GET("patient/search")
        Call<PatientsData> getPatients(@HeaderMap HashMap<String,String> header, @Query("name") String name );

        @GET("patient/{id}")
        Call<PatientProfileData> getProfile(@HeaderMap HashMap<String,String> header, @Path("id") String s);

        @PUT("patient/")
        Call<JSONObject> updateProfile(@HeaderMap HashMap<String,String> header, @Body UpdateProfileData updateProfile);

        @POST("patientvisit/")
        Call<JSONObject> addVisit(@HeaderMap HashMap<String,String > header, @Body NewVisitorData visitorData);

        @GET("brand/search")
        Call<List<BrandData>> getBrands(@HeaderMap HashMap<String,String> header, @Query("name") String name);

        @GET("dosage/search")
        Call<List<DosageData>> getDosage(@HeaderMap HashMap<String ,String > header,@Query("name") String name);

        @GET("patientvisit/search/{patientid}/{doctorid}")
        Call<List<VisitorHistoryData>> getHistory(@HeaderMap HashMap<String,String> header,
                                                  @Path("patientid") String id,@Path("doctorid") String docid);
        @GET("Prescription/search/{prescriptionid}")
        Call<PrescriptionDtO> getPrescription(@HeaderMap HashMap<String ,String > header, @Path("prescriptionid") String id);

        @GET("requestedAppointments/search/{searcheddate}/{doctorid}")
        Call<List<AppointmentsData>> getAppointmentsForDate(@HeaderMap HashMap<String,String> header,@Path("searcheddate") String date,@Path("doctorid") String doctorId);

        @PUT("requestedAppointments/")
        Call<JSONObject> postAppointment(@HeaderMap HashMap<String ,String > header, @Body PostAppointmentData postAppointmentData);

        @GET("employee/search/{id}")
        Call<EmployeeData> getEmpProfile(@HeaderMap HashMap<String ,String > header,@Path("id") String id);

    }


    public RestApiService apiService = null;

    public RestApiService api(){
        return apiService !=null ? apiService : getRetrofit(new EcEndPointDefinition()).create(RestApiService.class);
    }
}
