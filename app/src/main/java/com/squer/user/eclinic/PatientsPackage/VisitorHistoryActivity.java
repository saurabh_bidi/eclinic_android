package com.squer.user.eclinic.PatientsPackage;

import android.content.Context;
import android.content.Intent;
import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squer.user.eclinic.Entity.VisitorDataPackage.HistoryPresLines;
import com.squer.user.eclinic.Entity.VisitorDataPackage.VisitorHistoryData;
import com.squer.user.eclinic.R;
import com.squer.user.eclinic.Rest.RestClient;
import com.squerlib.standard.AbstractAsyncActivity;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VisitorHistoryActivity extends AbstractAsyncActivity {
    private String patientId,doctorId;
    private String prescriptionId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitor_history);
        patientId = getIntent().getExtras().getString("Patient Id");
        doctorId = getIntent().getExtras().getString("Doctor Id");
        getVisitHistory();
        onBackImagePressed();
        onAddPressed();
    }

    public void getVisitHistory(){
        try{
            showProgressDialog("Loading visit history",true);
            RestClient historyRestclient = RestClient.getInstance(getApplicationContext());
            Call<List<VisitorHistoryData>> jsonArrayCall = historyRestclient.api().getHistory(historyRestclient.getHeaderMap(),patientId,doctorId);
            jsonArrayCall.enqueue(new Callback<List<VisitorHistoryData>>() {
                @Override
                public void onResponse(Call<List<VisitorHistoryData>> call, Response<List<VisitorHistoryData>> response) {
                    if(response.isSuccessful()){
                        dismissProgressDialog();
                       HistoryAdapter historyAdapter = new HistoryAdapter(getApplicationContext(),response.body());
                        ListView historyList = (ListView) findViewById(R.id.history_list);
                        historyList.setAdapter(historyAdapter);
                    }else {
                        toast("Error");
                        dismissProgressDialog();
                    }
                }

                @Override
                public void onFailure(Call<List<VisitorHistoryData>> call, Throwable t) {
                    dismissProgressDialog();
                    t.printStackTrace();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void onBackImagePressed(){
        ImageView backImg = (ImageView) findViewById(R.id.back_history);
        backImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    public void onAddPressed(){
        ImageView addImg = (ImageView) findViewById(R.id.add_visit_history);
        addImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),VisitorActivity.class);
                intent.putExtra("Doctor Id",doctorId);
                intent.putExtra("Patient Id",patientId);
                startActivity(intent);
                finish();
            }
        });
    }

    //Adapter1
    public class HistoryAdapter extends ArrayAdapter <VisitorHistoryData> {
        public HistoryAdapter(Context context, List<VisitorHistoryData> responseData) {
            super(context, R.layout.history_custom_layout, responseData);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

            View listItemView = convertView;
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.history_custom_layout,parent,false);

            final VisitorHistoryData jsonResponse = getItem(position);
            TextView visitDate = listItemView.findViewById(R.id.date_text_history);
            TextView nextVisitDate = listItemView.findViewById(R.id.next_visit_date_history);
            TextView prevHistory = listItemView.findViewById(R.id.prevhis_text_history);
            TextView remarks = listItemView.findViewById(R.id.remarks_text_history);
            prescriptionId = jsonResponse.getPrescription().getReference().getId();

            try{
                //Format Date
                final java.text.SimpleDateFormat inputFormat = new java.text.SimpleDateFormat("yyyy-MM-dd");
                java.text.SimpleDateFormat outputFormat = new java.text.SimpleDateFormat("dd MMMM yyyy");
                String dateOfVisit = null;
                if (jsonResponse != null) {
                    dateOfVisit = jsonResponse.getDateOfVisit();
                }
                try {
                    Date date = inputFormat.parse(dateOfVisit);
                    String  parsedDate = outputFormat.format(date);
                    visitDate.setText(parsedDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }catch (Exception e){
                e.printStackTrace();
            }

           try {
               //Format Date
               java.text.SimpleDateFormat inputFormat1 = new java.text.SimpleDateFormat("yyyy-MM-dd");
               java.text.SimpleDateFormat outputFormat1 = new java.text.SimpleDateFormat("dd MMMM yyyy");
               String nextVisit = null;
               if (jsonResponse != null) {
                   nextVisit = jsonResponse.getNextVisitOn();
               }
               try {
                   Date date = inputFormat1.parse(nextVisit);
                   String  parsedDate = outputFormat1.format(date);
                   nextVisitDate.setText(parsedDate);
               } catch (ParseException e) {
                   e.printStackTrace();
               }
           }catch (Exception e){
                e.printStackTrace();
           }


            prevHistory.setText(jsonResponse.getPrevHistory());
            remarks.setText(jsonResponse.getRemarks());

            Button prescriptionBtn = listItemView.findViewById(R.id.pres_btn);
            prescriptionBtn.setTag(prescriptionId);

            prescriptionBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    prescriptionId = (String) v.getTag();

                    Intent intent = new Intent(getApplicationContext(), PrescriptionActivity.class);
                    intent.putExtra("Prescription Id",prescriptionId);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getContext().startActivity(intent);
                }
            });

            return listItemView;
        }
    }
}