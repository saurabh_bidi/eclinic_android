package com.squer.user.eclinic.Entity.FeedDataPackage;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by user on 10/27/2017.
 */

public class FeedPostsData {

    @SerializedName("id")
    private int id;

    @SerializedName("type")
    private String type;

    @SerializedName("slug")
    private String slug;

    @SerializedName("url")
    private String url;

    @SerializedName("status")
    private String status;

    @SerializedName("title")
    private String title;

    @SerializedName("title_plain")
    private String title_plain;

    @SerializedName("content")
    private String content;

    @SerializedName("excerpt")
    private String excerpt;

    @SerializedName("date")
    private String date;

    @SerializedName("modified")
    private String modified;

    private List<FeedsCategoriesData> categories;

    private List<FeedTagsData> tags;

    private FeedsAuthorData author;

    private List<FeedCommentsData> comments;

    private List<FeedAttachmentsData> attachments;

    @SerializedName("comment_count")
    private int comment_count;

    @SerializedName("comment_status")
    private String comment_status;

    private FeedsCustomData custom_fields;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle_plain() {
        return title_plain;
    }

    public void setTitle_plain(String title_plain) {
        this.title_plain = title_plain;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getExcerpt() {
        return excerpt;
    }

    public void setExcerpt(String excerpt) {
        this.excerpt = excerpt;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public List<FeedsCategoriesData> getCategories() {
        return categories;
    }

    public void setCategories(List<FeedsCategoriesData> categories) {
        this.categories = categories;
    }

    public List<FeedTagsData> getTags() {
        return tags;
    }

    public void setTags(List<FeedTagsData> tags) {
        this.tags = tags;
    }

    public FeedsAuthorData getAuthor() {
        return author;
    }

    public void setAuthor(FeedsAuthorData author) {
        this.author = author;
    }

    public List<FeedCommentsData> getComments() {
        return comments;
    }

    public void setComments(List<FeedCommentsData> comments) {
        this.comments = comments;
    }

    public List<FeedAttachmentsData> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<FeedAttachmentsData> attachments) {
        this.attachments = attachments;
    }

    public int getComment_count() {
        return comment_count;
    }

    public void setComment_count(int comment_count) {
        this.comment_count = comment_count;
    }

    public String getComment_status() {
        return comment_status;
    }

    public void setComment_status(String comment_status) {
        this.comment_status = comment_status;
    }

    public FeedsCustomData getCustom_fields() {
        return custom_fields;
    }

    public void setCustom_fields(FeedsCustomData custom_fields) {
        this.custom_fields = custom_fields;
    }
}
