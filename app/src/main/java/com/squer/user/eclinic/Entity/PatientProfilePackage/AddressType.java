package com.squer.user.eclinic.Entity.PatientProfilePackage;

/**
 * Created by user on 11/17/2017.
 */

public class AddressType {
    private String id;
    private String displayName;

    public AddressType(String id,String displayName){
        this.id = id;
        this.displayName = displayName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }
}
