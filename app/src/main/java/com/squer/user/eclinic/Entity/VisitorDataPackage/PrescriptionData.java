package com.squer.user.eclinic.Entity.VisitorDataPackage;

import java.util.List;

/**
 * Created by user on 11/18/2017.
 */

public class PrescriptionData {

    private String diagnosis;
    private String onExamination;
    private Double weight;
    private Double height;
    private String comments;
    private List<PrescriptionLine> lines;

    public PrescriptionData(String diagnosis,String onExamination,
                            Double weight,Double height,String comments,List<PrescriptionLine> lines){
        this.diagnosis=diagnosis;
        this.onExamination=onExamination;
        this.weight=weight;
        this.height=height;
        this.comments=comments;
        this.lines=lines;
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getOnExamination() {
        return onExamination;
    }

    public void setOnExamination(String onExamination) {
        this.onExamination = onExamination;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public List<PrescriptionLine> getLines() {
        return lines;
    }

    public void setLines(List<PrescriptionLine> lines) {
        this.lines = lines;
    }
}
