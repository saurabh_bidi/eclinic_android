package com.squer.user.eclinic.Entity.PatientProfilePackage;

import com.squer.user.eclinic.Entity.VisitorDataPackage.BrandReference;
import com.squer.user.eclinic.Entity.VisitorDataPackage.DosageReference;

/**
 * Created by user on 11/29/2017.
 */

public class LineDTO {
    private BrandReference brandId;
    private DosageReference dosageId;
    private String instructions;

   
    public BrandReference getBrandId() {
        return brandId;
    }

    public void setBrandId(BrandReference brandId) {
        this.brandId = brandId;
    }

    public DosageReference getDosageId() {
        return dosageId;
    }

    public void setDosageId(DosageReference dosageId) {
        this.dosageId = dosageId;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }
}
