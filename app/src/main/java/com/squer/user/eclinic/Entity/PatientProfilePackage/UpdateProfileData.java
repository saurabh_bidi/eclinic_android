package com.squer.user.eclinic.Entity.PatientProfilePackage;

import java.util.Date;

/**
 * Created by user on 11/17/2017.
 */

public class UpdateProfileData {
    private ProfReference reference;
    private String name;
    private String profileImage;
    private String contactNo1;
    private String contactNo2;
    private String email;
    private PatientUpdateAddress address;
    private String dateOfBirth;
    private int age;
    private double weight;
    private double height;
    private String gender;
    private String  maritalStatus;
    private String firstName;
    private String middleName;
    private String lastName;

    public UpdateProfileData(ProfReference reference,String name,String profileImage,
                             String contactNo1,String contactNo2 ,String email,PatientUpdateAddress address,String dateOfBirth,int age,double weight,double height,
                             String gender,String maritalStatus,String firstName,String middleName,String lastName)
    {
        this.reference=reference;
        this.name=name;
        this.profileImage=profileImage;
        this.contactNo1=contactNo1;
        this.contactNo2=contactNo2;
        this.email=email;
        this.address=address;
        this.dateOfBirth=dateOfBirth;
        this.age=age;
        this.weight=weight;
        this.height=height;
        this.gender=gender;
        this.maritalStatus=maritalStatus;
        this.firstName=firstName;
        this.middleName=middleName;
        this.lastName=lastName;

    }

    public ProfReference getReference() {
        return reference;
    }

    public void setReference(ProfReference reference) {
        this.reference = reference;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getContactNo1() {
        return contactNo1;
    }

    public void setContactNo1(String contactNo1) {
        this.contactNo1 = contactNo1;
    }

    public String getContactNo2() {
        return contactNo2;
    }

    public void setContactNo2(String contactNo2) {
        this.contactNo2 = contactNo2;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public PatientUpdateAddress getAddress() {
        return address;
    }

    public void setAddress(PatientUpdateAddress address) {
        this.address = address;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
