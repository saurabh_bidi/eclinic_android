package com.squer.user.eclinic.Entity.VisitorDataPackage;

/**
 * Created by user on 11/18/2017.
 */

public class PrescriptionLine {

    private BrandReference brand;
    private DosageReference  dosage;
    private String instructions;

    public PrescriptionLine(BrandReference brand,DosageReference dosage,String instructions){

        this.brand=brand;
        this.dosage = dosage;
        this.instructions=instructions;
    }

    public BrandReference getBrand() {
        return brand;
    }

    public void setBrand(BrandReference brand) {
        this.brand = brand;
    }

    public DosageReference getDosage() {
        return dosage;
    }

    public void setDosage(DosageReference dosage) {
        this.dosage = dosage;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }
}
