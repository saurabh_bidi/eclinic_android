package com.squer.user.eclinic.Entity.VisitorDataPackage;

/**
 * Created by user on 11/24/2017.
 */

public class BrandDTO {
    private BrandReference reference;
    private String instructions;

    public BrandDTO(BrandReference reference,String instructions){
        this.reference = reference;
        this.instructions = instructions;
    }

    public BrandReference getReference() {
        return reference;
    }

    public void setReference(BrandReference reference) {
        this.reference = reference;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }
}
