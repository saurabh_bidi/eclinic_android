package com.squer.user.eclinic.Entity.VisitorDataPackage;

/**
 * Created by user on 11/25/2017.
 */

public class LineDTO {
    private String dosage;
    private String instructions;
    private String brandname;

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getBrandname() {
        return brandname;
    }

    public void setBrandname(String brandname) {
        this.brandname = brandname;
    }
}
