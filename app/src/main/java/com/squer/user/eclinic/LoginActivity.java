package com.squer.user.eclinic;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.squer.user.eclinic.Entity.PatientDetailspackage.UserEntity;
import com.squer.user.eclinic.Rest.RestClient;
import com.squerlib.rest.LoginRequest;
import com.squerlib.standard.AbstractAsyncActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AbstractAsyncActivity {
    private Button loginBtn;
    public EditText username,password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        if (ContextCompat.checkSelfPermission(LoginActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(LoginActivity.this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    1);
        }
        if (ContextCompat.checkSelfPermission(LoginActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(LoginActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    1);

        }
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        loginBtn = (Button) findViewById(R.id.loginbutton);
        onLoginPressed();
    }

    public void onLoginPressed(){

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    if(!isNetworkAvailable()){
                        toast("Please Check Your Network Connection.");
                        return;
                    }
                    if(TextUtils.isEmpty(username.getText()))
                    {
                        toast("Username Can't be Empty");
                        return;
                    }
                    if(TextUtils.isEmpty(password.getText()))
                    {
                        toast("Password Can't be Empty");
                        return;
                    }
                    showProgressDialog("Logging in...",false);
                    try {
                        final RestClient restClient = RestClient.getInstance(getApplicationContext());
                        LoginRequest loginRequest = new LoginRequest(username.getText().toString(),
                                password.getText().toString());
                        Call<UserEntity> jsonObjectCall = restClient.api().requestLogin(loginRequest);
                        jsonObjectCall.enqueue(new Callback<UserEntity>() {
                            @Override
                            public void onResponse(Call<UserEntity> call, Response<UserEntity> response) {
                                if(response.isSuccessful()){
                                    dismissProgressDialog();
                                    restClient.addHeader("AUTH_CERTIFICATE",response.body().getCertificate());
                                    Intent intent = new Intent(getApplicationContext(),HomeActivity.class);
                                    intent.putExtra("Doctor Id",response.body().getOwnerId());
                                    intent.putExtra("Name",response.body().getName());
                                    startActivity(intent);
                                }else {
                                    dismissProgressDialog();
                                    toast("Failed");
                                }
                            }

                            @Override
                            public void onFailure(Call<UserEntity> call, Throwable t) {
                                t.printStackTrace();
                                dismissProgressDialog();
                            }
                        });
                    }catch (Exception e){
                        e.printStackTrace();
                    }

            }
        });
    }
}
