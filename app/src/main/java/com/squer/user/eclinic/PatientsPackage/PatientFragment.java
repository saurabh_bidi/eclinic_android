package com.squer.user.eclinic.PatientsPackage;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squer.user.eclinic.Entity.PatientDetailspackage.PatientsData;
import com.squer.user.eclinic.Entity.PatientDetailspackage.PatientsListData;
import com.squer.user.eclinic.R;
import com.squer.user.eclinic.Rest.RestClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientFragment extends Fragment {
    private View v;
    private EditText searchView;
    private String doctorId,patientId;
    private Button searchBtn;
    private ListAdapter patientsAdapter;
    private ListView patientsList;
    private ProgressDialog dialog;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.patient_fragment,container,false);
        searchView = (EditText) v.findViewById(R.id.searchBar);
        doctorId = getArguments().getString("Doctor Id");
        onSearchClicked();
        return v;
    }

    public void getPatientsData(){
        dialog = new ProgressDialog(getActivity());
        dialog.setMessage("Loading..");
        dialog.setCancelable(true);
        dialog.show();
        final RestClient patientsRestClient = RestClient.getInstance(getActivity());
        Call<PatientsData> jsonObjectCall = patientsRestClient.api().getPatients(patientsRestClient.getHeaderMap(),searchView.getText().toString());
        jsonObjectCall.enqueue(new Callback<PatientsData>() {
            @Override
            public void onResponse(Call<PatientsData> call, Response<PatientsData> response) {
                if(dialog.isShowing()){
                    dialog.dismiss();
                }
                patientsAdapter = new PatientAdapter(getContext(),response.body().getData());
                patientsList = (ListView) v.findViewById(R.id.patients_list);
                patientsList.setAdapter(patientsAdapter);
            }

            @Override
            public void onFailure(Call<PatientsData> call, Throwable t) {
                if(dialog.isShowing()){
                    dialog.dismiss();
                }
                t.printStackTrace();
            }
        });
    }

    public void onSearchClicked(){
        searchBtn = (Button) v.findViewById(R.id.search_btn);
        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPatientsData();
            }
        });
    }

   //Adapter
    public class PatientAdapter extends ArrayAdapter<PatientsListData> {
        public PatientAdapter(Context context, List<PatientsListData> responseData) {
            super(context, R.layout.custom_patients_list, responseData);
        }

        @NonNull
        @Override
        public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
            PatientsListData patientData = getItem(position);
            View listItemView = convertView;
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.custom_patients_list,parent,false);

            final PatientsListData jsonResponse = getItem(position);
            patientId = jsonResponse.getReference().getId();
            TextView name = (TextView) listItemView.findViewById(R.id.patient_name);
            name.setText(jsonResponse.getName());

            TextView contact = (TextView) listItemView.findViewById(R.id.patient_contact);
            contact.setText(jsonResponse.getContactNo1());

            TextView editProfile = (TextView) listItemView.findViewById(R.id.edit_profile);
            editProfile.setTag(patientId);

            editProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    patientId = (String) v.getTag();
                    Intent intent = new Intent(getContext(),EditProfileActivity.class);
                    intent.putExtra("Patient Id",patientId);
                    getContext().startActivity(intent);
                    Toast.makeText(getContext(),patientId,Toast.LENGTH_LONG).show();
                }
            });

            TextView Newvisit = (TextView) listItemView.findViewById(R.id.new_visit);
            Newvisit.setTag(patientId);
            Newvisit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    patientId = (String) v.getTag();
                    Intent intent = new Intent(getContext(),VisitorActivity.class);
                    intent.putExtra("Doctor Id",doctorId);
                    intent.putExtra("Patient Id",patientId);
                    getContext().startActivity(intent);
                }
            });

            TextView visitorHistory = (TextView) listItemView.findViewById(R.id.visitor_text);
            visitorHistory.setTag(patientId);
            visitorHistory.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    patientId = (String) v.getTag();
                    Intent intent = new Intent(getContext(),VisitorHistoryActivity.class);
                    intent.putExtra("Doctor Id",doctorId);
                    intent.putExtra("Patient Id",patientId);
                    getContext().startActivity(intent);
                }
            });

            return listItemView;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPatientsData();
    }
}
