package com.squer.user.eclinic.Entity.AppointmentsDataPackage;

import java.util.Date;

/**
 * Created by user on 11/27/2017.
 */

public class AppointmentsData {
    private Date appointmentdate;
    private String appointmentid;
    private String status_id;
    private String employee_id;
    private String employee_name;
    private String clinic_id;
    private String clinic_name;
    private String patient_id;
    private String patient_name;
    private String  app_start_time;
    private String  app_end_time;
    private String patientid;

    public Date getAppointmentdate() {
        return appointmentdate;
    }

    public void setAppointmentdate(Date appointmentdate) {
        this.appointmentdate = appointmentdate;
    }

    public String getAppointmentid() {
        return appointmentid;
    }

    public void setAppointmentid(String appointmentid) {
        this.appointmentid = appointmentid;
    }

    public String getStatus_id() {
        return status_id;
    }

    public void setStatus_id(String status_id) {
        this.status_id = status_id;
    }

    public String getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(String employee_id) {
        this.employee_id = employee_id;
    }

    public String getEmployee_name() {
        return employee_name;
    }

    public void setEmployee_name(String employee_name) {
        this.employee_name = employee_name;
    }

    public String getClinic_id() {
        return clinic_id;
    }

    public void setClinic_id(String clinic_id) {
        this.clinic_id = clinic_id;
    }

    public String getClinic_name() {
        return clinic_name;
    }

    public void setClinic_name(String clinic_name) {
        this.clinic_name = clinic_name;
    }

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public String getPatient_name() {
        return patient_name;
    }

    public void setPatient_name(String patient_name) {
        this.patient_name = patient_name;
    }

    public String getApp_start_time() {
        return app_start_time;
    }

    public void setApp_start_time(String app_start_time) {
        this.app_start_time = app_start_time;
    }

    public String getApp_end_time() {
        return app_end_time;
    }

    public void setApp_end_time(String app_end_time) {
        this.app_end_time = app_end_time;
    }

    public String getPatientid() {
        return patientid;
    }

    public void setPatientid(String patientid) {
        this.patientid = patientid;
    }
}
