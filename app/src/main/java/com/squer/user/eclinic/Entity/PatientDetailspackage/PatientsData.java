package com.squer.user.eclinic.Entity.PatientDetailspackage;

import java.util.List;

/**
 * Created by user on 11/16/2017.
 */

public class PatientsData {

    private int pageNo;
    private int totalPages;
    private int totalRecords;
    private List<PatientsListData> data;

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(int totalRecords) {
        this.totalRecords = totalRecords;
    }

    public List<PatientsListData> getData() {
        return data;
    }

    public void setData(List<PatientsListData> data) {
        this.data = data;
    }
}
