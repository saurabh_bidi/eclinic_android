package com.squer.user.eclinic.SettingsPackage;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.EditText;

import com.squer.user.eclinic.Entity.EmployeeDataPackage.EmployeeData;
import com.squer.user.eclinic.R;
import com.squer.user.eclinic.Rest.RestClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EmployeeProfileActivity extends AppCompatActivity {
    private String doctorId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_profile);
        setTitle("My Profile");
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        doctorId = getIntent().getExtras().getString("Doctor Id");
        getProfile();
    }

    public void getProfile(){
        RestClient profileClient = RestClient.getInstance(getApplicationContext());
        Call<EmployeeData> jsonObjectCall = profileClient.api().getEmpProfile(profileClient.getHeaderMap(),doctorId);
        jsonObjectCall.enqueue(new Callback<EmployeeData>() {
            @Override
            public void onResponse(Call<EmployeeData> call, Response<EmployeeData> response) {
                EmployeeData responseData  = response.body();
                EditText nameText = (EditText) findViewById(R.id.profile_name);
                nameText.setText(responseData.getName());

                EditText email = (EditText) findViewById(R.id.profile_email_id);
                email.setText(responseData.getEmail());

                EditText qualification = (EditText) findViewById(R.id.profile_designation);
                qualification.setText(responseData.getQualification());

                EditText landline = (EditText) findViewById(R.id.profile_landline);
                landline.setText(responseData.getLandLine());
            }

            @Override
            public void onFailure(Call<EmployeeData> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }
}
