package com.squerlib.annotation;

import com.squerlib.Realm.DBTablePK;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import io.realm.RealmModel;
import io.realm.RealmObject;

/**
 * Created by Thatsjm4u on 9/8/2017.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Entity {
    Class<? extends RealmObject> RealmTable();
}
