package com.squerlib;

import com.squerlib.Realm.DBEntityPK;
import com.squerlib.session.UserSessionEntity;

/**
 * Created by Thatsjm4u on 9/13/2017.
 */

public abstract class UserParentEntity extends DBEntityPK implements UserSessionEntity {

    /*
    {"preferences":null,
    "tenantId":"TENAT00000000000000000000000000000001",
    "certificate":"2YpMssV72uCLW2SLQGwo1kqnDXbxk3Bdhsa6DS0Ulq3/p1knD7OuazU/+ljvFswzMA9+eIEkoB5NaMkc6bUAra8RrI6BBH4U0tDCBigXOToxctps93RXF4tM0hy8Zr+dWwxMJfi/rj7oh9i/WyZ75+8f0RP4TwkFX6RUJ5UQ4Ns78OXWG5cEVtaIUbzH61FoSw/qXJVy9BA=",
    "userName":"TESTTM",
    "name":"TESTTM",
    "id":"EMPLY0000005d7c8dfc0152dffd2d4f005d9c"}
    * */

    private String certificate;
    private String tenantId;

    //    private String preferences;
    private String userName;
    private String id;
    private String name;
    private boolean loggedIn;
    private String ipAddress;
    private String jobRole;
//    private String dateOfJoining;
    private String locationId;
    private String divisionId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTenantId() {
        return tenantId;
    }

    public void setTenantId(String tenantId) {
        this.tenantId = tenantId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getLocationId() {
        return locationId;
    }

    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    public String getDivisionId() {
        return divisionId;
    }

    public void setDivisionId(String divisionId) {
        this.divisionId = divisionId;
    }

    public void setJobRole(String jobRole) {
        this.jobRole = jobRole;
    }

    public String getJobRole() {
        return jobRole;
    }
}
