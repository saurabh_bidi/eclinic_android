package com.squerlib.CustomWidget;

import android.content.Context;
import android.text.Editable;
import android.util.AttributeSet;

import com.squerlib.CustomListeners.TextWatcherExtendedListener;

/**
 * Created by Thatsjm4u on 10/26/2017.
 */

public class EditTextExtended extends android.support.v7.widget.AppCompatEditText {
    private TextWatcherExtendedListener mListeners = null;

    public EditTextExtended(Context context) {
        super(context);
    }

    public EditTextExtended(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EditTextExtended(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void addTextChangedListener(TextWatcherExtendedListener watcher) {
        if (mListeners == null) {
            mListeners = watcher;
        }
    }

    public void removeTextChangedListener(TextWatcherExtendedListener watcher) {
        if (mListeners != null) {
            mListeners = null;
        }
    }

    void sendBeforeTextChanged(CharSequence text, int start, int before, int after) {
        if (mListeners != null) {
            mListeners.beforeTextChanged(this, text, start, before, after);
        }
    }

    void sendOnTextChanged(CharSequence text, int start, int before, int after) {
        if (mListeners != null) {
            mListeners.onTextChanged(this, text, start, before, after);
        }
    }

    void sendAfterTextChanged(Editable text) {
        if (mListeners != null) {
            mListeners.afterTextChanged(this, text);
        }
    }
}