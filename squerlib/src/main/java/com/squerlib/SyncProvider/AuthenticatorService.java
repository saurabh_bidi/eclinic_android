package com.squerlib.SyncProvider;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by Thatsjm4u on 10/9/2017.
 */

public abstract class AuthenticatorService extends Service {

    // Instance field that stores the authenticator object
    private Authenticator mAuthenticator;

    /**
     *
     *  Add To AndroidManifest.xml
         <service android:name="AuthServiceName" >
             <intent-filter>
             <action android:name="android.accounts.AccountAuthenticator" />
             </intent-filter>

             <meta-data
             android:name="android.accounts.AccountAuthenticator"
             android:resource="@xml/authenticator" />
         </service>
     */
    @Override
    public void onCreate() {
        // Create a new authenticator object
        mAuthenticator = new Authenticator(this);
        //android.os.Debug.waitForDebugger();
    }
    /*
     * When the system binds to this Service to make the RPC call
     * return the authenticator's IBinder.
     */
    @Override
    public IBinder onBind(Intent intent) {
        return mAuthenticator.getIBinder();
    }


}
