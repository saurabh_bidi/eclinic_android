package com.squerlib.SyncProvider;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;

import com.squerlib.R;
import com.squerlib.SquerApplication;
import com.squerlib.rest.AbstractRestClient;

/**
 * Created by Thatsjm4u on 10/7/2017.
 */

public abstract class SyncAdapter extends AbstractThreadedSyncAdapter {

    private Context context;
    private AbstractRestClient restClient;

    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        this.context = context;
    }

//    @Override
//    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {
//        SquerService service;
//        SyncRecord syncRecord = null;
//        try {
//            service = new ModelService(getContext());
//            syncRecord = new SyncRecord(System.currentTimeMillis(), 0);
//            service.save(syncRecord);
//            Log.d(TAG, "SyncAdapter.onPerformSync() started..");
//            MasterSyncExecutor executor = new MasterSyncExecutor(context);
//            executor.execute();
//            syncRecord.setStatus("SUCCESS");
//            syncRecord.setEndTime(System.currentTimeMillis());
//            service.save(syncRecord);
//        } catch (Exception e) {
//            if (syncRecord != null) {
//                syncRecord.setStatus("ERROR");
//                syncRecord.setMessage(e.getMessage());
//                syncRecord.setEndTime(System.currentTimeMillis());
//            }
//            Log.e(TAG, "Error while performing sync", e);
//        } finally {
//            service = null;
//        }
//    }

    /**
     * Helper method to have the sync adapter sync immediately
     *
     * @param context The context used to access the account service
     */
    public static void syncImmediately(Context context, String mAuthority) {
        // Pass the settings flags by inserting them in a bundle
        Bundle bundle = new Bundle();
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);

        /*
         * Request the sync for the default account, authority, and
         * manual sync settings
         */
        ContentResolver.requestSync(getSyncAccount(context), mAuthority, bundle);
    }

    /**
     * Helper method to get the fake account to be used with SyncAdapter, or make a new one
     * if the fake account doesn't exist yet.  If we make a new account, we call the
     * onAccountCreated method so we can initialize things.
     *
     * @param context The context used to access the account service
     * @return a fake account.
     */
    public static Account getSyncAccount(Context context) {
        // Get an instance of the Android account manager
        AccountManager accountManager = (AccountManager) context.getSystemService(Context.ACCOUNT_SERVICE);

        // Create the account type and default account
        Account newAccount = new Account(context.getString(R.string.app_name), context.getString(R.string.sync_account_type));


        if(SquerApplication.getInstance().hasPermission("android.permission.AUTHENTICATE_ACCOUNTS"))

        {
        // If the password doesn't exist, the account doesn't exist
            if (null == accountManager.getPassword(newAccount)) {

        /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
                if (!accountManager.addAccountExplicitly(newAccount, "", null)) {
                    return null;
                }
            /*
             * If you don't set android:syncable="true" in
             * in your <provider> element in the manifest,
             * then call ContentResolver.setIsSyncable(account, AUTHORITY, 1)
             * here.
             */


            }
        }
        return newAccount;
    }


}
