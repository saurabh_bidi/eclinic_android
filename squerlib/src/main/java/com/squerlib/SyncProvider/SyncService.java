package com.squerlib.SyncProvider;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/**
 * Created by Thatsjm4u on 10/9/2017.
 */

public abstract class SyncService extends Service {
    private static final String TAG = "SyncService";
    private static final Object sSyncAdapterLock = new Object();
    private static SyncAdapter sSyncAdapter = null;

    /**
     * Add Service to AndroidManifest.xml File
     *
              <service
                     android:name="ServiceName"
                     android:exported="true" >
                     <intent-filter>
                     <action android:name="android.content.SyncAdapter" />
                     </intent-filter>

                     <meta-data
                     android:name="android.content.SyncAdapter"
                     android:resource="@xml/syncadapter" />

              </service>
     * */
    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate - SyncService: Called");
        synchronized (sSyncAdapterLock) {
            if (sSyncAdapter == null)
                sSyncAdapter = getsSyncAdapter(getApplicationContext(), true);
            //android.os.Debug.waitForDebugger();
        }
        Log.d(TAG, "onCreate - SyncService: Finished");
    }

    public abstract <SA extends SyncAdapter> SyncAdapter getsSyncAdapter(Context context, boolean autoInitialize);

    @Override
    public IBinder onBind(Intent intent) {
        return sSyncAdapter.getSyncAdapterBinder();
    }
}
