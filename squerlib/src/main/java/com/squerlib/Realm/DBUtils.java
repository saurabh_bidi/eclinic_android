package com.squerlib.Realm;

import com.squerlib.annotation.Entity;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.realm.Realm;
import io.realm.RealmModel;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.exceptions.RealmException;

/**
 * Created by Thatsjm4u on 9/8/2017.
 */

public class DBUtils implements DBMethods {

    private static String TAG = "Squer.DBUtils";
    public static final String WHERE = " WHERE ";
    public static final String CONDITION = " = ? ";
    public static final String CONDITION_LIKE = " LOWER({columnName}) LIKE ? ";
    public static final String AND_ATTR = " AND ";
    public static final String OR_ATTR = " OR ";
    public static final String LIMIT = " LIMIT ";
    public static final String OFFSET = " OFFSET ";
    public static final String NOTCONDITION = " <> ? ";
    public static final String ISNOTNULL = " IS NOT NULL ";
    private String orderBy;
    private String order;
    private String limit;
    private String offset;
    private ArrayList<OperationContainer> operationMap;
    private Realm.Transaction transaction;

    public DBUtils() {}

    @Override
    public void begainTransation() {
        this.operationMap = new ArrayList<>();
        transaction = null;
    }

    @Override
    public void commitTransation () throws RealmException {
        /* TODO: Complete all the transations here and make transation object null
        *
        *       IMPORTANT : Realm Instance must be dependent on type of execution , on which thread user wants it to execute.
        * */
        createOperationforTransaction();
        if(transaction != null)
        {
            Realm realm = Realm.getDefaultInstance();
            try {
            realm.executeTransaction(transaction);
            } finally {
                if(realm != null)
                    realm.close();
                transaction = null;
                operationMap = null;
            }
        }
    }

    @Override
    public void commitTransation(final Realm.Transaction.OnSuccess SuccessCallBack, final Realm.Transaction.OnError errorCallBack) {
        /* TODO: Complete all the transations here and make transation object null
        *
        *       IMPORTANT : Realm Instance must be dependent on type of execution , on which thread user wants it to execute.
        * */

        createOperationforTransaction();
        if(transaction != null)
        {
                    final Realm realm = Realm.getDefaultInstance();
                    Realm.Transaction.OnSuccess onSuccess = new Realm.Transaction.OnSuccess() {
                        @Override
                        public void onSuccess() {
                            if(realm != null)   realm.close();
                            transaction = null;
                            operationMap = null;
                            SuccessCallBack.onSuccess();
                        }
                    };

                    Realm.Transaction.OnError onError = new Realm.Transaction.OnError() {
                        @Override
                        public void onError(Throwable error) {
                            error.printStackTrace();
                            if(realm != null)   realm.close();
                            transaction = null;
                            operationMap = null;
                            errorCallBack.onError(error);
                        }
                    };
                    realm.executeTransactionAsync(transaction, onSuccess, onError);
        }
    }

    @Override
    public boolean isCommited() {
        return transaction == null;
    }

    public static Set<Field> findFields(Class<?> classs, Class<? extends Annotation> ann) {
        Set<Field> set = new HashSet<>();
        Class<?> c = classs;
        while (c != null) {
            for (Field field : c.getDeclaredFields()) {
                if (field.isAnnotationPresent(ann)) {
                    set.add(field);
                }
            }
            c = c.getSuperclass();
        }
        return set;
    }

    /*  ***********************************************       BUNCH OPERATIONS            ************************************************/

    @Override
    public <E extends DBEntityPK, DBT extends RealmObject> void createAllInsideTransation(List<E> dataObjects) {
        if(dataObjects!= null && dataObjects.size() == 0 || operationMap == null) return;
            Entity entity = dataObjects.get(0).getClass().getAnnotation(Entity.class);
             for(E e : dataObjects) {
                 DBT dbt = e.getRowInstance();
                    operationMap.add(new OperationContainer(OperationContainer.operation.INSERT, dbt));
             }
    }

    @Override
    public <E extends DBEntityPK, DBT extends RealmObject> void UpdateAllInsideTransation(List<E> dataObjects) {
        if(dataObjects!= null && dataObjects.size() == 0 || operationMap == null) return;
        Entity entity = dataObjects.get(0).getClass().getAnnotation(Entity.class);
        for(E e : dataObjects) {
            DBT dbt = e.getRowInstance();
            operationMap.add(new OperationContainer(OperationContainer.operation.UPDATE, dbt));
        }
    }

    @Override
    public <E extends DBEntityPK, DBT extends RealmObject> void deleteAllInsideTransation(List<E> dataObjects) {
        if(dataObjects!= null && dataObjects.size() == 0 || operationMap == null) return;
        for(E e : dataObjects) {
            DBT dbt = e.getRowInstance();
            operationMap.add(new OperationContainer(OperationContainer.operation.DELETE, dbt));
        }
    }

    @Override
    public void createOperationforTransaction(){

        transaction = new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                for(OperationContainer container : operationMap) {
                    container.ExecuteOperation(realm);
                }
            }
        };

    }

    /*  ***********************************************       SINGLE OPERATIONS            ************************************************/

    @Override
    public <E extends DBEntityPK> void createSingleInsideTransation(E userEntity) {
            if(operationMap != null)
            {
                operationMap.add(new OperationContainer(OperationContainer.operation.INSERT, userEntity.getRowInstance()));
            }
    }

    @Override
    public <E extends DBEntityPK> void updateSingleInsideTransation(E userEntity) {
            if(operationMap != null)
            {
                operationMap.add(new OperationContainer(OperationContainer.operation.UPDATE, userEntity.getRowInstance()));
            }
    }

    @Override
    public <E extends DBEntityPK> void deleteSingleInsideTransation(E userEntity) {
            if(operationMap != null)
            {
                operationMap.add(new OperationContainer(OperationContainer.operation.DELETE, userEntity.getRowInstance()));
            }
    }

    @Override
    public <E extends DBEntityPK> void deleteAllInsideTransation(Class<E> clazz) {
            if(operationMap != null)
            {
                operationMap.add(new OperationContainer(OperationContainer.operation.EMPTYTABLE, clazz));
            }
    }

/*******************************************************************    SINGLETON OPERATIONS WITH TRANSACTIONS   *******************************************************************/

    /**
     * <t/> Should not be used with Transaction, already Contains transaction operation may cause Exception.
     * */
    @Override
    public <E extends DBEntityPK> long createSingle(E dataObject) {
        return createOrUpdateSingle(dataObject);
    }

    /**
     * <t/> Should not be used with Transaction, already Contains transaction operation may cause Exception.
     * */
    @Override
    public <E extends DBEntityPK> long updateSingle(E dataObject ){
        return createOrUpdateSingle(dataObject);
    }

    /**
     * <t/> Should not be used with Transaction, already Contains transaction operation may cause Exception.
     * */
    @Override
    public <E extends DBEntityPK> long createOrUpdateSingle(final E dataObject) {
        final long[] temp = {0};
            final Realm realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                        if (dataObject.isValidSquerId()) {
                            DBTablePK realmObj = (DBTablePK) realm.copyToRealmOrUpdate((RealmModel) dataObject.getRowInstance());
                            if (realmObj != null && ((DBTablePK)realmObj).isValidSquerId())
                                temp[0] = realmObj.getSquerId();
                        } else {
                            long squerId = 1;
                            Entity entity = dataObject.getClass().getAnnotation(Entity.class);
                            if(realm.where(entity.RealmTable()).max("squerId") != null) squerId += realm.where(entity.RealmTable()).max("squerId").longValue();
                            DBTablePK realmObj = (DBTablePK) realm.createObject(entity.RealmTable(), squerId);
                            realmObj.copyFrom(dataObject);
                            if (realmObj != null && realmObj.isValidSquerId())
                                temp[0] = realmObj.getSquerId();
                        }
                }
            });
        realm.close();
        return temp[0];
    }

    /**
     * <t/> Should not be used with Transaction, already Contains transaction operation may cause Exception.
     * */
    @Override
    public <E extends DBEntityPK> void deleteSingle (final E dataObject) throws Exception {
        if(dataObject.isValidSquerId()) {
            Realm realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    dataObject.getRowInstance().deleteFromRealm();
                }
            });
            realm.close();
        } else throw new Exception();
    }

    /**
     * <t/> Should not be used with Transaction, already Contains transaction operation may cause Exception.
     * */
    @Override
    public <E extends DBEntityPK> ArrayList<E> findAll(final SquerCriteria squerCriteria) {
        return squerCriteria.findAllsync();
    }

    /**
     * <t/> Should not be used with Transaction, already Contains transaction operation may cause Exception.
     * */
    @Override
    public <E extends DBEntityPK> ArrayList<E> findAll(Class<E> clazz) {
        return new SquerCriteria<>(clazz).findAll();
    }

    /**
     * <t/> Should not be used with Transaction, already Contains transaction operation may cause Exception.
     * */
    @Override
    public <E extends DBEntityPK> E findOne(Class<? extends DBEntityPK> clazz, SquerCriteria<? extends DBEntity> squerCriteria) {
        Entity entity = clazz.getAnnotation(Entity.class);
        return (E) squerCriteria.findFirst();
    }

    /**
     * <t/> Should not be used with Transaction, already Contains transaction operation may cause Exception.
     * */
    @Override
    public void deleteAllEntries(final Class<? extends DBEntityPK> clazz) {

        Realm realm = null;
        try{
            realm = Realm.getDefaultInstance();
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    Entity entity = clazz.getAnnotation(Entity.class);
                    realm.delete(entity.RealmTable());
                }
            });
        }finally {
            if(null != realm)
                realm.close();
        }
    }

    /**
     * <b> PRECAUTION </b>
     * <br/> Should not be used with <b>Transaction</b>, already Contains transaction operation may cause Exception.
     * <p/> <b>OPERATION IMPACT</b> <br/> It will delete all the data stored in this DataBase.
     *
     * */
    @Override
    public void deleteAll() {
        Realm realm = null;
        try{
            realm = Realm.getDefaultInstance();
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                                realm.deleteAll();
                    }
                });
        }finally {
            if(null != realm)
            realm.close();
        }
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
    }

    @Override
    public <E extends DBEntityPK, DBT extends DBTablePK> ArrayList<E> findSorted(SquerCriteria<E> squerCriteria) {

        RealmResults<? extends RealmModel> items = squerCriteria.findAllSorted();
        ArrayList<E> eList = new ArrayList<>();
        for ( RealmModel e : items )
        {
            DBT dbt = (DBT) e;
            eList.add((E) ((DBT) e).getEntity());
        }
        return eList;
    }

    @Override
    public <E extends DBEntityPK> void deleteAllSelected(final SquerCriteria<E> squerCriteria) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                squerCriteria.getRealmQuery().findAll().deleteAllFromRealm();
            }
        });
    }
}
