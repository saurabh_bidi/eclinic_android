package com.squerlib.Realm;

import java.io.Serializable;

import io.realm.Realm;
import io.realm.RealmModel;
import io.realm.RealmObject;

/**
 * Created by Thatsjm4u on 9/8/2017.
 */

public interface DBEntity  extends Serializable{

    public Long getSquerId();
    public void setSquerId(Long squerId);
    public <E extends RealmObject> E getRowInstance();
//    <E extends DBTablePK> long createTouple(Class<? extends RealmModel> realmTable, String classname);
}
