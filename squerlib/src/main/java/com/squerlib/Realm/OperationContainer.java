package com.squerlib.Realm;

import com.squerlib.annotation.Entity;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;

/**
 * Created by Thatsjm4u on 9/24/2017.
 */

public class OperationContainer<DBT extends RealmObject> {

    private Class clazz;

    public enum operation{
        INSERT,
        UPDATE,
        DELETE,
        EMPTYTABLE,
    }

    private operation operation;
    private DBT DBObject;

    public <E extends DBEntityPK> OperationContainer(operation op, Class<E> clazz) {
            this.operation = op;
            this.clazz = clazz.getAnnotation(Entity.class).RealmTable();
    }

    public void ExecuteOperation(Realm realm) {

        switch (operation) {
            case INSERT:
            case UPDATE:
                            {
                               long squerId = 1;

                                    if (((DBTablePK) DBObject).getSquerId() != null && ((DBTablePK) DBObject).isValidSquerId())
                                            realm.copyToRealmOrUpdate(DBObject);
                                    else
                                    {
                                        if( realm.where(DBObject.getClass()).max("squerId") != null)
                                        {
                                            squerId += realm.where(DBObject.getClass()).max("squerId").longValue();
                                        }
                                            ((DBTablePK) DBObject).setSquerId(squerId);
                                            realm.copyToRealmOrUpdate(DBObject);
                                    }
                            }
                                break;
            case DELETE:
                            DBObject.deleteFromRealm();
                    break;

            case EMPTYTABLE:
                            {
                                if(this.clazz != null)
                                    realm.delete(this.clazz);
                            }
                    break;

            default:
        }

    }

    public OperationContainer(OperationContainer.operation operation, DBT DBObject) {
        this.operation = operation;
        this.DBObject = DBObject;
    }

    public OperationContainer.operation getOperation() {
        return operation;
    }

    public void setOperation(OperationContainer.operation operation) {
        this.operation = operation;
    }

    public DBT getDBObject() {
        return DBObject;
    }

    public void setDBObject(DBT DBObject) {
        this.DBObject = DBObject;
    }

}

