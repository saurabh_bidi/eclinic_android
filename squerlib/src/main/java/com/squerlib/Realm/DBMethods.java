package com.squerlib.Realm;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.exceptions.RealmException;

/**
 * Created by Thatsjm4u on 9/24/2017.
 */

public interface DBMethods {

    void begainTransation();

    void commitTransation() throws RealmException;

    void commitTransation(Realm.Transaction.OnSuccess SuccessCallBack, Realm.Transaction.OnError errorCallBack);

    boolean isCommited();

    <E extends DBEntityPK, DBT extends RealmObject> void createAllInsideTransation(List<E> dataObjects);

    <E extends DBEntityPK, DBT extends RealmObject> void UpdateAllInsideTransation(List<E> dataObjects);

    <E extends DBEntityPK, DBT extends RealmObject> void deleteAllInsideTransation(List<E> userEntityList);

    void createOperationforTransaction();

    <E extends DBEntityPK> void createSingleInsideTransation(E userEntity);

    <E extends DBEntityPK> void updateSingleInsideTransation(E userEntity);

    <E extends DBEntityPK> void deleteSingleInsideTransation(E userEntity);

    <E extends DBEntityPK> void deleteAllInsideTransation(Class<E> clazz);

    public <E extends DBEntityPK> long createSingle(E dataObject);

    public <E extends DBEntityPK> long updateSingle(E dataObject );

    <E extends DBEntityPK> long createOrUpdateSingle(E dataObject);

    public <E extends DBEntityPK> void deleteSingle (E dataObject) throws Exception;

    <E extends DBEntityPK> ArrayList<E> findAll(SquerCriteria squerCriteria) ;

    <E extends DBEntityPK> ArrayList<E> findAll(Class<E> clazz) ;

    <E extends DBEntityPK> E findOne(Class<? extends DBEntityPK> clazz, SquerCriteria<? extends DBEntity> squerCriteria);

    void deleteAllEntries(Class<? extends DBEntityPK> clazz);

    void deleteAll();

    <E extends DBEntityPK, DBT extends DBTablePK> ArrayList<E> findSorted(SquerCriteria<E> squerCriteria);

    <E extends DBEntityPK> void deleteAllSelected(SquerCriteria<E> squerCriteria);
}
