package com.squerlib.Realm;

import android.support.annotation.Nullable;
import android.util.Log;

import com.squerlib.annotation.Column;
import com.squerlib.annotation.Entity;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmModel;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;


/**
 * Created by Thatsjm4u on 9/11/2017.
 */

public class SquerCriteria<SC extends DBEntity> {

    private final Class<SC> DBEClazz;
    Class<? extends RealmModel> DBTClazz;
    RealmQuery<? extends RealmModel> realmQuery;
    String fieldName = "squerId";
    Sort sortOrder = Sort.ASCENDING;

    RealmQuery<? extends RealmModel> getRealmQuery() {
        return realmQuery;
    }

    void setRealmQuery(RealmQuery<? extends RealmModel> realmQuery) {
        this.realmQuery = realmQuery;
    }

    public SquerCriteria(Class<SC> clazz) {
            DBEClazz = clazz;
            Entity entity =  clazz.getAnnotation(Entity.class);
            DBTClazz = entity.RealmTable();
            realmQuery = RealmQuery.createQuery(Realm.getDefaultInstance(), DBTClazz);
    }

    public SquerCriteria isNull(String fieldName) {
        realmQuery.isNull(getActualTableColoumName(fieldName));
       return this;
    }

    public SquerCriteria isNotNull(String fieldName) {
        realmQuery.isNotNull(getActualTableColoumName(fieldName));
        return this;
    }

    public SquerCriteria equalTo(String fieldName, @Nullable String value) {
        realmQuery.equalTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria equalTo(String fieldName, @Nullable Byte value) {
        realmQuery.equalTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria equalTo(String fieldName, @Nullable byte[] value) {
        realmQuery.equalTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria equalTo(String fieldName, @Nullable Short value) {
        realmQuery.equalTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria equalTo(String fieldName, @Nullable Integer value) {
        realmQuery.equalTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria equalTo(String fieldName, @Nullable Long value) {
        realmQuery.equalTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria equalTo(String fieldName, @Nullable Double value) {
        realmQuery.equalTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria equalTo(String fieldName, @Nullable Float value) {
        realmQuery.equalTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria equalTo(String fieldName, @Nullable Boolean value) {
        realmQuery.equalTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria equalTo(String fieldName, @Nullable Date value) {
        realmQuery.equalTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria in(String fieldName, String[] values) {
        realmQuery.in(getActualTableColoumName(fieldName), values);
        return this;
    }

    public SquerCriteria in(String fieldName, Byte[] values) {
        realmQuery.in(getActualTableColoumName(fieldName), values);
        return this;
    }

    public SquerCriteria in(String fieldName, Short[] values) {
        realmQuery.in(getActualTableColoumName(fieldName), values);
        return this;
    }

    public SquerCriteria in(String fieldName, Integer[] values) {
        realmQuery.in(getActualTableColoumName(fieldName), values);
        return this;
    }

    public SquerCriteria in(String fieldName, Long[] values) {
        realmQuery.in(getActualTableColoumName(fieldName), values);
        return this;
    }

    public SquerCriteria in(String fieldName, Double[] values) {
        realmQuery.in(getActualTableColoumName(fieldName), values);
        return this;
    }

    public SquerCriteria in(String fieldName, Float[] values) {
        realmQuery.in(getActualTableColoumName(fieldName), values);
        return this;
    }

    public SquerCriteria in(String fieldName, Boolean[] values) {
        realmQuery.in(getActualTableColoumName(fieldName), values);
        return this;
    }

    public SquerCriteria in(String fieldName, Date[] values) {
        realmQuery.in(getActualTableColoumName(fieldName), values);
        return this;
    }

    public SquerCriteria notEqualTo(String fieldName, @Nullable String value) {
        realmQuery.notEqualTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria notEqualTo(String fieldName, @Nullable Byte value) {
        realmQuery.notEqualTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria notEqualTo(String fieldName, @Nullable byte[] value) {
        realmQuery.notEqualTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria notEqualTo(String fieldName, @Nullable Short value) {
        realmQuery.notEqualTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria notEqualTo(String fieldName, @Nullable Integer value) {
        realmQuery.notEqualTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria notEqualTo(String fieldName, @Nullable Long value) {
        realmQuery.notEqualTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria notEqualTo(String fieldName, @Nullable Double value) {
        realmQuery.notEqualTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria notEqualTo(String fieldName, @Nullable Float value) {
        realmQuery.notEqualTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria notEqualTo(String fieldName, @Nullable Boolean value) {
        realmQuery.notEqualTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria notEqualTo(String fieldName, @Nullable Date value) {
        realmQuery.notEqualTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria greaterThan(String fieldName, int value) {
        realmQuery.greaterThan(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria greaterThan(String fieldName, long value) {
        realmQuery.greaterThan(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria greaterThan(String fieldName, double value) {
        realmQuery.greaterThan(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria greaterThan(String fieldName, float value) {
        realmQuery.greaterThan(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria greaterThan(String fieldName, Date value) {
        realmQuery.greaterThan(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria greaterThanOrEqualTo(String fieldName, int value) {
        realmQuery.greaterThanOrEqualTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria greaterThanOrEqualTo(String fieldName, long value) {
        realmQuery.greaterThanOrEqualTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria greaterThanOrEqualTo(String fieldName, double value) {
        realmQuery.greaterThanOrEqualTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria greaterThanOrEqualTo(String fieldName, float value) {
        realmQuery.greaterThanOrEqualTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria greaterThanOrEqualTo(String fieldName, Date value) {
        realmQuery.greaterThanOrEqualTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria lessThan(String fieldName, int value) {
        realmQuery.lessThan(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria lessThan(String fieldName, long value) {
        realmQuery.lessThan(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria lessThan(String fieldName, double value) {
        realmQuery.lessThan(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria lessThan(String fieldName, float value) {
        realmQuery.lessThan(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria lessThan(String fieldName, Date value) {
        realmQuery.lessThan(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria lessThanOrEqualTo(String fieldName, int value) {
        realmQuery.lessThanOrEqualTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria lessThanOrEqualTo(String fieldName, long value) {
        realmQuery.lessThanOrEqualTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria lessThanOrEqualTo(String fieldName, double value) {
        realmQuery.lessThanOrEqualTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria lessThanOrEqualTo(String fieldName, float value) {
        realmQuery.lessThanOrEqualTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria lessThanOrEqualTo(String fieldName, Date value) {
        realmQuery.lessThanOrEqualTo(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria between(String fieldName, int from, int to) {
        realmQuery.between(getActualTableColoumName(fieldName), from, to);
        return this;
    }

    public SquerCriteria between(String fieldName, long from, long to) {
        realmQuery.between(getActualTableColoumName(fieldName), from, to);
        return this;
    }

    public SquerCriteria between(String fieldName, double from, double to) {
        realmQuery.between(getActualTableColoumName(fieldName), from, to);
        return this;
    }

    public SquerCriteria between(String fieldName, float from, float to) {
        realmQuery.between(getActualTableColoumName(fieldName), from, to);
        return this;
    }

    public SquerCriteria between(String fieldName, Date from, Date to) {
        realmQuery.between(getActualTableColoumName(getActualTableColoumName(fieldName)), from, to);
        return this;
    }

    public SquerCriteria contains(String fieldName, String value) {
        realmQuery.contains(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria beginsWith(String fieldName, String value) {
        realmQuery.beginsWith(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria endsWith(String fieldName, String value) {
        realmQuery.endsWith(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria like(String fieldName, String value) {
        realmQuery.like(getActualTableColoumName(fieldName), value);
        return this;
    }

    public SquerCriteria like(String fieldName, String value, Case casing) {
        realmQuery.like(getActualTableColoumName(fieldName), value, casing);
        return this;
    }

    public SquerCriteria beginGroup() {
        realmQuery.beginGroup();
        return this;
    }

    public SquerCriteria endGroup() {
        realmQuery.endGroup();
        return this;
    }

    public SquerCriteria or() {
        realmQuery.or();
        return this;
    }

    public SquerCriteria not() {
        realmQuery.not();
        return this;
    }

    public SquerCriteria isEmpty(String fieldName) {
        realmQuery.isEmpty(getActualTableColoumName(fieldName));
        return this;
    }

    public SquerCriteria isNotEmpty(String fieldName) {
        realmQuery.isNotEmpty(getActualTableColoumName(fieldName));
        return this;
    }

    public SquerCriteria setSortingOn(String fieldName) {
        this.fieldName = fieldName;
        return this;
    }

    public SquerCriteria setSortOrder(Sort sortOrder) {
        this.sortOrder = sortOrder;
        return this;
    }

    public Number sum(String fieldName) {
        return realmQuery.sum(getActualTableColoumName(fieldName));
    }

    public double average(String fieldName) {
        return realmQuery.average(getActualTableColoumName(fieldName));
    }

    public Number min(String fieldName) {
        return realmQuery.min(getActualTableColoumName(fieldName));
    }

    public Date minimumDate(String fieldName) {
        return realmQuery.minimumDate(getActualTableColoumName(fieldName));
    }

    public Number max(String fieldName) {
        return realmQuery.max(getActualTableColoumName(fieldName));
    }

    public Date maximumDate(String fieldName) {
        return realmQuery.maximumDate(getActualTableColoumName(fieldName));
    }

    public long count() {
        return realmQuery.count();
    }

//    public SquerCriteria distinct(String fieldName) {
//        realmQuery.distinct(getActualTableColoumName(fieldName));
//        return this;
//    }
//
//    public RealmResults<E> distinctAsync(String fieldName) {
//        realm.checkIfValid();
//
//        realm.sharedRealm.capabilities.checkCanDeliverNotification(ASYNC_QUERY_WRONG_THREAD_MESSAGE);
//        SortDescriptor distinctDescriptor = SortDescriptor.getInstanceForDistinct(getSchemaConnector(), query.getTable(), fieldName);
//        return createRealmResults(query, null, distinctDescriptor, false);
//    }
//
//    public RealmResults<E> distinct(String firstFieldName, String... remainingFieldNames) {
//        realm.checkIfValid();
//
//        String[] fieldNames = new String[1 + remainingFieldNames.length];
//
//        fieldNames[0] = firstFieldName;
//        System.arraycopy(remainingFieldNames, 0, fieldNames, 1, remainingFieldNames.length);
//        SortDescriptor distinctDescriptor = SortDescriptor.getInstanceForDistinct(getSchemaConnector(), table, fieldNames);
//        return createRealmResults(query, null, distinctDescriptor, true);
//    }

    public ArrayList<SC> findAll() {
        ArrayList<SC> arrayList = new ArrayList<>();
        List<DBTablePK> list = (List<DBTablePK>) realmQuery.findAll();
        if(list != null) {
            for(DBTablePK dbTable : list)
                arrayList.add((SC) dbTable.getEntity());
        }
        return arrayList;
    }
//
//    public RealmResults<E> findAllAsync() {
//        realm.checkIfValid();
//
//        realm.sharedRealm.capabilities.checkCanDeliverNotification(ASYNC_QUERY_WRONG_THREAD_MESSAGE);
//        return createRealmResults(query, null, null, false);
//    }
//
    public RealmQuery<?> sorted() {
        return realmQuery;
    }
//
//    public RealmResults<E> findAllSortedAsync(final String fieldName, final Sort sortOrder) {
//        realm.checkIfValid();
//
//        realm.sharedRealm.capabilities.checkCanDeliverNotification(ASYNC_QUERY_WRONG_THREAD_MESSAGE);
//        SortDescriptor sortDescriptor = SortDescriptor.getInstanceForSort(getSchemaConnector(), query.getTable(), fieldName, sortOrder);
//        return createRealmResults(query, sortDescriptor, null, false);
//    }
//
//
//    public RealmResults<E> findAllSorted(String fieldName) {
//        return findAllSorted(getActualTableColoumName(fieldName), Sort.ASCENDING);
//    }
//
//    public RealmResults<E> findAllSortedAsync(String fieldName) {
//        return findAllSortedAsync(getActualTableColoumName(fieldName), Sort.ASCENDING);
//    }
//
//    public RealmResults<E> findAllSorted(String[] fieldNames, Sort[] sortOrders) {
//        realm.checkIfValid();
//
//        SortDescriptor sortDescriptor = SortDescriptor.getInstanceForSort(getSchemaConnector(), query.getTable(), fieldNames, sortOrders);
//        return createRealmResults(query, sortDescriptor, null, true);
//    }
//
//    private boolean isDynamicQuery() {
//        return className != null;
//    }
//
//    public RealmResults<E> findAllSortedAsync(String[] fieldNames, final Sort[] sortOrders) {
//        realm.checkIfValid();
//
//        realm.sharedRealm.capabilities.checkCanDeliverNotification(ASYNC_QUERY_WRONG_THREAD_MESSAGE);
//        SortDescriptor sortDescriptor = SortDescriptor.getInstanceForSort(getSchemaConnector(), query.getTable(), fieldNames, sortOrders);
//        return createRealmResults(query, sortDescriptor, null, false);
//    }
//
//    public RealmResults<E> findAllSorted(String fieldName1, Sort sortOrder1,
//                                         String fieldName2, Sort sortOrder2) {
//        return findAllSorted(new String[] {fieldName1, fieldName2}, new Sort[] {sortOrder1, sortOrder2});
//    }
//
//    public RealmResults<E> findAllSortedAsync(String fieldName1, Sort sortOrder1,
//                                              String fieldName2, Sort sortOrder2) {
//        return findAllSortedAsync(new String[] {fieldName1, fieldName2}, new Sort[] {sortOrder1, sortOrder2});
//    }

    private String getClassName (Class<?> enclosingClass) {
        if (enclosingClass != null) {
            return (enclosingClass.getName());
        } else {
            return (getClass().getName());
        }
    }

    ArrayList<SC> findAllsync() {
        ArrayList<SC> scs = new ArrayList<>();
        List<DBTablePK> list = (List<DBTablePK>) realmQuery.findAllAsync();
        if(list != null) {
            for(DBTablePK dbTable : list)
                scs.add((SC) dbTable.getEntity());
        }
        return scs;
    }

    SC findFirst() {
        DBTablePK abc = (DBTablePK) realmQuery.findFirst();
            if(abc != null)
                    return (SC) abc.getEntity();
            else
                    return null;
    }

    public RealmResults<? extends RealmModel> findAllSorted() {
        return realmQuery.findAllSorted(getActualTableColoumName(fieldName), sortOrder);
    }

    private String getActualTableColoumName(String TableColoumName) {

        try {
            Field f = DBEClazz.getDeclaredField(TableColoumName);
            if(f.getAnnotation(Column.class) != null)
            {
                String s = f.getAnnotation(Column.class).toString();
                return s.substring(s.lastIndexOf('=')+1, s.lastIndexOf(')'));
            }
            return TableColoumName;
        } catch (NoSuchFieldException e) {
            if(!TableColoumName.equals("squerId"))
            e.printStackTrace();
            return TableColoumName;
        }
    }

}
