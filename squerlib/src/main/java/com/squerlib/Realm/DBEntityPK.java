package com.squerlib.Realm;

import android.support.annotation.Nullable;

import com.squerlib.annotation.Column;

import io.realm.Realm;
import io.realm.RealmModel;

/**
 * Created by Thatsjm4u on 9/8/2017.
 */

public abstract class DBEntityPK implements DBEntity {

    @Column(TableColummnName = "squerId")
    protected Long squerId;

    @Nullable
    public Long getSquerId() {
        return squerId;
    }
    public void setSquerId(Long squerId) {
        this.squerId = squerId;
    }

    public boolean isValidSquerId() {
        return squerId != null && squerId > 0;
    }
/*
    @Override
    public <E extends DBTablePK> long createTouple(Class<? extends RealmModel> realmTable, String classname) {
        long temp = 0;
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        long squerId = 1;
        if(realm.where(realmTable).max("squerId") != null) squerId += realm.where(realmTable).max("squerId").longValue();
        E tempTable = (E) realm.createObject(realmTable, squerId);
        temp = tempTable.copyFrom(this);
        realm.commitTransaction();
        return temp;
    }
*/


}
