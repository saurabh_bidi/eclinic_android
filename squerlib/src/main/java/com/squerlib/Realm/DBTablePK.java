package com.squerlib.Realm;

import io.realm.RealmModel;
import io.realm.annotations.PrimaryKey;

/**
 * Created by Thatsjm4u on 9/8/2017.
 */

public interface DBTablePK extends RealmModel{

    /*
    *       Create A PrimaryKey Attribute Named As squerId
    * */
    public abstract Long getSquerId();

    public abstract void setSquerId(Long squerId);

    /**
     *      copy & replace the return statment
     *
     *      return squerId != null && squerId > 0;
     * */
    public abstract boolean isValidSquerId();

    public abstract Long copyFrom(DBEntityPK Object);

    public abstract DBEntityPK getEntity();

}
