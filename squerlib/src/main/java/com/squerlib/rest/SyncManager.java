//package com.squerlib.rest;
//
//import android.content.Context;
//import android.util.Log;
//import android.widget.Toast;
//
//import com.google.gson.Gson;
//import com.google.gson.JsonArray;
//import com.google.gson.JsonElement;
//import com.google.gson.JsonObject;
//import com.squerlib.session.SquerSessionManager;
//
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//
//import io.realm.Realm;
//import io.realm.RealmObject;
//import io.realm.exceptions.RealmPrimaryKeyConstraintException;
//import retrofit.Callback;
//import retrofit.RetrofitError;
//import retrofit.client.Response;
//
///**
// * Created by Thatsjm4u on 8/18/2017.
// */
//
//public class SyncManager {
//
//    static int SyncLock = 0;
//
//    public void upload(final Context context, final long id, final CallbackClass callbackClass ) throws Exception {
//
//        try{
//            final Realm realm = Realm.getDefaultInstance();
//            RestClient restClient = new RestClient(context, RestClient.type.PICBOOK_HTTP);
//            PicBookRealm picBookRealm = realm.where(PicBookRealm.class).equalTo("id", id).equalTo("isSynced", false).findFirst();
//            if(picBookRealm!= null)
//            {
//                JsonElement str = new Gson().toJsonTree(picBookRealm.getEntity());
//                JsonObject jsonObject = str.getAsJsonObject();
//                jsonObject.remove("isSynced");
//                jsonObject.remove("referenceId");
//                jsonObject.remove("id");
//                Log.d(TAG, jsonObject.getAsString());
//                restClient.api().SyncSingle(jsonObject, new Callback<JsonObject>() {
//                    @Override
//                    public void success(JsonObject jsonObject, Response response) {
//                        try {
//                            Realm realm = Realm.getDefaultInstance();
//                            realm.beginTransaction();
//                            PicBookRealm picBookRealm = realm.where(PicBookRealm.class).equalTo("id", id).findFirst();
//                            picBookRealm.setId(jsonObject.get("id").getAsString());
//                            picBookRealm.setSynced(true);
//
//                            realm.where(ChemistRealm.class).equalTo("referenceId", picBookRealm.getOwner()).findFirst().setSynced(true);
//                            callbackClass.callbackSuccess();
//                        }catch (Exception e) {
//                            throw e;
//                        }finally {
//                            realm.commitTransaction();
//                        }
//                    }
//
//                    @Override
//                    public void failure(RetrofitError error) {
//                        callbackClass.callbackFailed();
//                    }
//                });
//            }
//
//        }catch (Exception e) {
//            e.printStackTrace();
//            throw e;
//        }
//    }
//
//    public void SyncAll (final Context context, final CallbackClass callbackClass) {
//
//        if(SyncLock > 0 ) {
//            Toast.makeText(context, "Sync Is Already in Progress.", Toast.LENGTH_LONG).show();
//            callbackClass.callbackFailed();
//            return;
//        }
//
//        final Realm realm = Realm.getDefaultInstance();
//        List<PicBookRealm> picBookRealmList = realm.where(PicBookRealm.class).equalTo("isSynced", false).findAll();
//        SyncLock = picBookRealmList.size();
//        final boolean isSpectra = (boolean) SquerSessionManager.getSquerSession(context).getmSessionData("isSpectra");
//
//        if(SyncLock > 0)
//        {
//            Toast.makeText(context, "Sync is in Progress.", Toast.LENGTH_LONG).show();
//            for (final PicBookRealm picBookRealm : picBookRealmList) {
//                try {
////                    upload(context, picBookRealm.getId(), new CallbackClass() {
////                        @Override
////                        public void callbackSuccess(PicBook picBook) {
////                        }
////
////                        @Override
////                        public void callbackSuccess() {
////
////                        }
////
////                        @Override
////                        public void callbackFailed() {
////
////                        }
////                    });
//                        RestClient restClient = new RestClient(context, RestClient.type.PICBOOK_HTTP);
//                        if(picBookRealm!= null &&
//                                    picBookRealm.getContent() != null && !picBookRealm.getContent().isEmpty() &&
//                                    picBookRealm.getEmail() != null && !picBookRealm.getEmail().isEmpty() &&
//                                    picBookRealm.getPhone() != null && !picBookRealm.getPhone().isEmpty()
//                                )
//                        {
//                            JsonElement str = new Gson().toJsonTree(picBookRealm.getEntity());
//                            JsonObject jsonObject = str.getAsJsonObject();
//                            jsonObject.remove("isSynced");
//                            jsonObject.remove("referenceId");
////                            jsonObject.remove("id");
//                            jsonObject.add("visitInfo", new JsonArray());
//                        if(isSpectra)
//                        {
//                            List<SpectraRealm> spectraRealms = realm.where(SpectraRealm.class)
//                                                                .equalTo("chemistId", picBookRealm.getOwner())
//                                                                .equalTo("isSynced", false)
//                                                                .findAll();
//                            JsonArray spectraElements = new JsonArray();
//                            for( SpectraRealm spectraRealm : spectraRealms)
//                            {
//                                String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(spectraRealm.getVisitDate());
//
//                                JsonElement jsonElement = new Gson().toJsonTree(spectraRealm.getEntity());
//
//                                JsonObject jsonObj = jsonElement.getAsJsonObject();
//                                jsonObj.remove("visitDate");
//                                jsonObj.remove("isSynced");
//                                jsonObj.remove("id");
//                                jsonObj.addProperty("visitDate", date);
//                                spectraElements.add(jsonObj);
//                            }
//                            jsonObject.add("visitInfo", spectraElements);
//                        }
//
//                            JsonArray array = new JsonArray();
//                            array.add(jsonObject);
//                            JsonObject jsonObject1 = new JsonObject();
//                            jsonObject1.add("stringData", array);
//                            restClient.api().SyncSingleNew(jsonObject1, new Callback<JsonObject>() {
//                                @Override
//                                public void success(JsonObject jsonObject, Response response) {
//                                        Realm realm1 = Realm.getDefaultInstance();
//                                    try {
//                                        realm1.beginTransaction();
//                                        picBookRealm.setId("0001");
//                                        picBookRealm.setSynced(true);
//                                        realm1.where(ChemistRealm.class).equalTo("referenceId", picBookRealm.getOwner()).findFirst().setSynced(true);
//
//                                        if(isSpectra)
//                                        {
//                                            List<SpectraRealm> spectraRealms = realm1.where(SpectraRealm.class)
//                                                    .equalTo("chemistId", picBookRealm.getOwner())
//                                                    .equalTo("isSynced", false)
//                                                    .findAll();
//                                            for( SpectraRealm spectraRealm : spectraRealms)
//                                            {
//                                                spectraRealm.setSynced(true);
//                                            }
//                                        }
//
//                                    }catch (Exception e) {
//                                        e.printStackTrace();
//                                    }finally {
//                                        realm1.commitTransaction();
//                                        SyncLock--;
//                                        if(SyncLock == 0) {
//                                            callbackClass.callbackSuccess();
//                                        }
//                                    }
//                                }
//
//                                @Override
//                                public void failure(RetrofitError error) {
//                                    error.printStackTrace();
//                                    SyncLock--;
//                                    if(SyncLock == 0) {
//                                        callbackClass.callbackFailed();
//                                    }
//                                }
//                            });
//                        } else {
////                            picBookRealm.deleteFromRealm();
//                        }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                        SyncLock--;
//                    if(SyncLock == 0) {
//                        callbackClass.callbackFailed();
//                    }
//                }
//            }
//        }
//        else
//        {
//            Toast.makeText(context, "There is Nothing to Sync, Please Make Some Entries.", Toast.LENGTH_LONG).show();
//            callbackClass.callbackFailed();
//        }
//    }
//
//    public void getPickbookData(Context context, String owner, final Chemist chemist, final CallbackClass callback) {
//        final RestClient restClient = new RestClient(context, RestClient.type.PICBOOK_HTTP);
//        restClient.api().getSingleImageDetails(owner, new Callback<JsonObject>() {
//            @Override
//            public void success(JsonObject jsonObject, Response response) {
//                final boolean isSpectra = (boolean) SquerSessionManager.getSquerSession(getInstance()).getmSessionData("isSpectra");
//
//                if(jsonObject != null && !jsonObject.get("byteContent").isJsonNull() &&
//                            !jsonObject.get("latitude").isJsonNull() &&
//                            !jsonObject.get("longitude").isJsonNull() &&
//                            !jsonObject.get("email").isJsonNull() &&
//                            !jsonObject.get("phone").isJsonNull())
//                {
//                    final PicBook picBook = new PicBook();
//                    picBook.setExt(jsonObject.get("type")!=null&&!jsonObject.get("type").isJsonNull()?jsonObject.get("type").getAsString(): ".jpg");
//                    picBook.setOwner(jsonObject.get("ownerId")!=null&&!jsonObject.get("ownerId").isJsonNull()&&!jsonObject.get("ownerId").getAsString().isEmpty()&&!jsonObject.get("ownerId").getAsString().equals("null")?jsonObject.get("ownerId").getAsString(): chemist.getId());
//                    picBook.setContent(jsonObject.get("byteContent")!=null&&!jsonObject.get("byteContent").isJsonNull()?jsonObject.get("byteContent").getAsString(): "");
//                    picBook.setLatitude(jsonObject.get("latitude")!=null&&!jsonObject.get("latitude").isJsonNull()?jsonObject.get("latitude").getAsDouble(): 0l);
//                    picBook.setLongitude(jsonObject.get("longitude")!=null&&!jsonObject.get("longitude").isJsonNull()?jsonObject.get("longitude").getAsDouble(): 0l);
//                    picBook.setEmail(jsonObject.get("email")!=null&&!jsonObject.get("email").isJsonNull()?jsonObject.get("email").getAsString(): "");
//                    picBook.setPhone(jsonObject.get("phone")!=null&&!jsonObject.get("phone").isJsonNull()?jsonObject.get("phone").getAsString(): "");
//                    picBook.setSynced(true);
//                    picBook.setId(jsonObject.get("id")!=null&&!jsonObject.get("id").isJsonNull()?jsonObject.get("id").getAsString(): "");
//                    if(isSpectra)
//                    {
//                          restClient.api().getSyncedSpectra(picBook.getOwner(), new Callback<JsonArray>() {
//                              @Override
//                              public void success(JsonArray jsonArray, Response response) {
//
//                                  Realm realm = Realm.getDefaultInstance();
//                                  long size = realm.where(SpectraRealm.class).count()+1;// .findAll().size();
//                                  for(JsonElement jsonElement : jsonArray)
//                                  {
//                                      try {
//                                          if (jsonElement.getAsJsonObject() != null) {
//                                              realm.beginTransaction();
//                                              Spectra spectra = new Spectra();
//                                              spectra.setChemistId(jsonElement.getAsJsonObject().get("chemist_id").getAsString());
//                                              spectra.setProductName(jsonElement.getAsJsonObject().get("product_name").getAsString());
//                                              spectra.setQuantity(jsonElement.getAsJsonObject().get("quantity").getAsLong());
//                                              spectra.setRobGift(jsonElement.getAsJsonObject().get("rob_gift").getAsString());
//                                              spectra.setStockistName(jsonElement.getAsJsonObject().get("stockist_name").getAsString());
//                                              Date date = new Date(jsonElement.getAsJsonObject().get("visit_date").getAsJsonObject().get("date").getAsLong());
//                                              spectra.setVisitDate(date);
//                                              spectra.setSynced(true);
//
//                                              SpectraRealm spectraRealm = realm.createObject(SpectraRealm.class, size);
//                                              spectraRealm.copyObject(spectra);
//                                              realm.commitTransaction();
//                                              size++;
//                                          }
//                                      }
//                                      catch (Exception e) {
//                                          e.printStackTrace();
//                                      }
//                                  }
//                              callback.callbackSuccess(picBook);
//                              }
//
//                              @Override
//                              public void failure(RetrofitError error) {
//                                    callback.callbackSuccess(picBook);
//                                    Toast.makeText(getInstance(), "Some Data Can't be Load", Toast.LENGTH_LONG);
//                              }
//                          });
//                    }
//                    else
//                    callback.callbackSuccess(picBook);
//                }else
//                    callback.callbackSuccess(null);
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                callback.callbackFailed();
//            }
//        });
//    }
//
////    public void getAllSpectraData(Context context, String owner, fin)
//
//    public interface CallbackClass{
//        void callbackSuccess(PicBook picBook);
//        void callbackSuccess();
//        void callbackFailed();
//    }
//
//    CallbackClass myCallbackClass;
//
//    void registerCallback(CallbackClass callbackClass){
//        myCallbackClass = callbackClass;
//    }
//
//    void doSomething(PicBook picBook){
////do something here
////call callbackSuccess method
//        myCallbackClass.callbackSuccess(picBook);
//    }
//
//    public void getAllPicbookData(Context context, final CallbackClass callbackClass) {
//
//        final Realm realm = Realm.getDefaultInstance();
//        List<ChemistRealm> chemistRealms = realm.where(ChemistRealm.class).findAll();
//        final int[] size = {chemistRealms.size()};
//
//        for (ChemistRealm chemistRealm: chemistRealms)
//        {
//            getPickbookData(context, chemistRealm.getId(), chemistRealm.getEntity(), new CallbackClass() {
//                @Override
//                public void callbackSuccess(PicBook picBook) {
//                    try
//                    {   size[0]--;
//                        realm.beginTransaction();
//                        PicBookRealm picBookRealm = new PicBookRealm();
//                        picBookRealm.copyObject(picBook);
//                        picBookRealm.setId(picBook.getId());
//                        realm.copyToRealmOrUpdate(picBookRealm);
//                        picBook = picBookRealm.getEntity();
//                    }
//                    finally {
//                        realm.commitTransaction();
//                    }
//                    if(size[0] == 0)
//                        callbackClass.callbackSuccess();
//                }
//
//                @Override
//                public void callbackSuccess() {
//
//                }
//
//                @Override
//                public void callbackFailed() {
//                    if(size[0] == 0)
//                        callbackClass.callbackFailed();
//                }
//            });
//        }
//    }
//
//    public void Save(Class<RealmObject> objectClass, RealmObject object) {
//
////        try{
////            Realm realm = Realm.getDefaultInstance();
////            realm.beginTransaction();
////            if(object.getId() > 0)
////            {
////                PicBookRealm picBookRealm = new PicBookRealm();
////                picBookRealm.copyObject(picBook);
////                picBookRealm.setId(picBook.getId());
////                getRealm().copyToRealmOrUpdate(picBookRealm);
////                picBook = picBookRealm.getEntity();
////            } else
////            {
////                PicBookRealm picBookRealm = getRealm().createObject(PicBookRealm.class, getRealm().where(PicBookRealm.class).count()+1);
////                picBookRealm.copyObject(picBook);
////                picBook = picBookRealm.getEntity();
////            }
//    }
//
//}
