package com.squerlib.rest;

import android.content.Context;

import com.squerlib.session.SquerSessionManager;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Retrofit;

/**
 * Created by Thatsjm4u on 9/14/2017.
 */

public interface RestCall {

    HashMap<String, String> headerMap = new HashMap<>();

    /**
         *    @inheritDocs
         *      Provide the end point to RestClient
         *
         */
    public String findEndPoint(RestEndPointDefinition endpoint);


    /**     @inheritDocs
     *
     *      Save the EndPoint detials to sharedPreference
     *
     */
    public void saveIps(Context context, RestEndPointDefinition endPoint);

    /**     @inheritDocs
     *
     *      Provide the {@link Retrofit},
     *      In Retrofit you can set
     *      LogLevel, RequestInterceptor, ErrorHandler etc...
     *
     */
    public <E extends SquerSessionManager> Retrofit getRetrofit(RestEndPointDefinition endpoint) throws Exception;


    /**
    *       Create A HashMap<Key,Value> object in SessionManager & implement related methods on it,
    *
    *       this hashmap work as the Header for all the (RestClient) HTTP Request
    **/
    HashMap<String, String> getHeaderMap();

    /**
    *       Clear the Map
    **/
    void clearHeaderMap();

    /**
    *       Delete the perticular entry, by its key
    **/
    void deleteHeaderEntry(String key);

    /**
    *       Add Entry to existing HashMap, in case map is null then create new map than enter details.
    **/
    void addHeader(String key, String value);

    /**
     *       Add Bulk Entries to existing HashMap, in case map is null then create new map than enter details.
     **/
    void addHeaders(Map<String, String> headers);
}
