package com.squerlib.rest;

import android.content.Context;
import android.content.SharedPreferences;
import com.google.gson.Gson;
import com.squerlib.R;
import com.squerlib.session.SquerSessionManager;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Thatsjm4u on 9/14/2017.
 */

public abstract class AbstractRestClient implements RestCall {

    protected final String TAG = getClass().getSimpleName();
    private String IP_URL;
    private String FEEDS_URL;

    /**
     * @inheritDocs   :                              WHAT TO DO in this Class?
     *
     *      Create A Interface inside this class & create API methods in it,
     *      & then call getRetrofit() to get retrofit & Call its Create() by passing Interface Class,
     *      Create() will retrun you a object of your Interface & that Object will help you to make API Request
     *
     **/
    protected AbstractRestClient(Context context) {
        this.IP_URL = context.getString(R.string.BASE_URL);

    }

    /**
     * @inheritDocs   :                              WHAT TO DO in this Class?
     *
     *      Create A Interface inside this class & create API methods in it,
     *      & then call getRetrofit() to get retrofit & Call its Create() by passing Interface Class,
     *      Create() will retrun you a object of your Interface & that Object will help you to make API Request
     *
     *      Base_Url_Format: format Type => 'http://{IP_ADDRESS}/webapp/rest/v1.0/'
     *
     **/
    public AbstractRestClient(Context context, int BaseUrlResource) {
        this.IP_URL = context.getString(BaseUrlResource);

    }

    /**
     * @param endpoint
     * @inheritDocs Provide the end point to RestClient
     */
    @Override
    public String findEndPoint(RestEndPointDefinition endpoint) {
        return IP_URL.replace("{IP_ADDRESS}", endpoint.getHost());

    }

    /**
     * @param context
     * @param endPoint
     * @inheritDocs Save the EndPoint detials to sharedPreference
     */
    @Override
    public void saveIps(Context context, RestEndPointDefinition endPoint) {
        SharedPreferences sharedPref = context.getSharedPreferences("infinity", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(endPoint.getPreferenceKey(), new Gson().toJson(endPoint));
//        editor.putString("BASE_URL_PICBOOK",PickbookServer);
        editor.commit();
    }

    /**
     * @param endpoint
     * @inheritDocs Provide the {@link Retrofit},
     * In Retrofit you can set
     * LogLevel, RequestInterceptor, ErrorHandler etc...
     */
    @Override
    public <E extends SquerSessionManager> Retrofit getRetrofit(RestEndPointDefinition endpoint){

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(interceptor);
        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(findEndPoint(endpoint))
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        return retrofit;
    }

    public class VersionMismatchException extends Throwable {
        public VersionMismatchException(String message){
            super(message);
        }
    }

    private class RestUrlConnectionClient extends OkHttpClient {

        /**
         * Default connect timeout (in milliseconds).
         */
        @Override
        public int connectTimeoutMillis() {
            return 180 * 1000; // super.connectTimeoutMillis();
        }

        /**
         * Default read timeout (in milliseconds).
         */
        @Override
        public int readTimeoutMillis() {
            return 180 * 1000; // super.readTimeoutMillis();
        }

        /**
         * Returns an immutable list of interceptors that observe the full span of each call: from before
         * the connection is established (if any) until after the response source is selected (either the
         * origin server, cache, or both).
         */
        @Override
        public List<Interceptor> interceptors() {
            return super.interceptors();
        }

    }

    @Override
    public HashMap<String, String> getHeaderMap() {
        return headerMap;
    }

    @Override
    public void clearHeaderMap() {
        headerMap.clear();
    }

    @Override
    public void deleteHeaderEntry(String key) {
        headerMap.remove(key);
    }

    @Override
    public void addHeader(String key, String value) {
        headerMap.put(key, value);
    }

    @Override
    public void addHeaders(Map<String, String> headers) {
        headerMap.putAll(headers);
    }

}
