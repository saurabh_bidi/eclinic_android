package com.squerlib.rest;

import android.content.Context;

/**
 * Created by Thatsjm4u on 9/14/2017.
 */

public interface RestEndPointDefinition {

    public String getPreferenceKey();
    public String getHost();
}
