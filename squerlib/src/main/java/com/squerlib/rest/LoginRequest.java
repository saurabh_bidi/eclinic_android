package com.squerlib.rest;

import java.io.Serializable;

/**
 * Created by Thatsjm4u on 8/11/2017.
 */

public class LoginRequest implements Serializable {

    private static final long serialVersionUID = 4048699292797921187L;

    private String username;
    private String password;

    public LoginRequest(){}

    public LoginRequest(String username, String password) {
        super();
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }
    public void setUserName(String username) {
        this.username = username;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

}

