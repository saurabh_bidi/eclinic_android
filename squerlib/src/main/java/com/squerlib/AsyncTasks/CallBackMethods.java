package com.squerlib.AsyncTasks;

/**
 * Created by Thatsjm4u on 9/12/2017.
 */

public interface CallBackMethods<Params, Progress, Result> {

    public void OnPreExecute();

    public Result DoInBackGround(Params... data);

    public void onPostExecute(Result result);

    public void onProgressUpdate(Progress... values);

    public void onCancelled(Result result);

    public void onCancelled();

    public void publishProgress(Progress... values);

}
