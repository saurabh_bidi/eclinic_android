package com.squerlib.AsyncTasks;

import java.util.ArrayList;

/**
 * Created by Thatsjm4u on 10/4/2017.
 */

public class SquenceProcessExecutor {

    ArrayList<Runnable> runnables;
    int executingTaskIndex = -1;

    public SquenceProcessExecutor(ArrayList<Runnable> runnables) {
        this.runnables = runnables;
    }

    public void execute() {
        ExecuteNext();
    }

    private void ExecuteNext() {
        if(executingTaskIndex < runnables.size())
        new OnNewThreadExecutor().execute(new Runnable() {
            @Override
            public void run() {
//                try {
                    for(Runnable runnable : runnables)
                    {
                        runnable.run();
                    }
//                    return;
//                } finally {
//                    ExecuteNext();
//                }
            }
        });
    }

}
