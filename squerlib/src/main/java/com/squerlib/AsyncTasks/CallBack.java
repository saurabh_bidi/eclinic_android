package com.squerlib.AsyncTasks;

/**
 * Created by Thatsjm4u on 9/23/2017.
 */

public interface CallBack<E> {
    public void onPostExecute(E e);
}
