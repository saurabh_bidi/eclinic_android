package com.squerlib.AsyncTasks;

import android.os.AsyncTask;

/**
 * Created by Thatsjm4u on 9/12/2017.
 */

public class Asynctask extends AsyncTask<Object, Object, Object> {

    private AsyncCallBacks asyncCallBacks = null;

    public Asynctask(AsyncCallBacks asyncCallBacks) {

            this.asyncCallBacks = asyncCallBacks;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        asyncCallBacks.OnPreExecute();
    }

    @Override
    protected Object doInBackground(Object... objects) {
        return this.asyncCallBacks.DoInBackGround(objects);
    }

    @Override
    protected void onPostExecute(Object o) {
        super.onPostExecute(o);
        this.asyncCallBacks.onPostExecute(o);
    }

    @Override
    protected void onProgressUpdate(Object... values) {
        super.onProgressUpdate(values);
        asyncCallBacks.onProgressUpdate(values);
    }

    @Override
    protected void onCancelled(Object o) {
        super.onCancelled(o);
        asyncCallBacks.onCancelled(o);
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        asyncCallBacks.onCancelled();
    }
}
