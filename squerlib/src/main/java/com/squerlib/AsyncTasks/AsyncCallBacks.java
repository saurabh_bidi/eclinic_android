package com.squerlib.AsyncTasks;

import java.util.List;

/**
 * Created by Thatsjm4u on 9/12/2017.
 */

    public abstract class AsyncCallBacks<Params, Progress, Result> implements CallBackMethods<Params, Progress, Result> {


    @Override
    public Result DoInBackGround(Params... data) {
        return null;
    }

    @Override
    public void onProgressUpdate(Progress... values) {

    }

    @Override
    public void onCancelled(Result result) {

    }

    @Override
    public void onCancelled() {

    }

    @Override
    public void publishProgress(Progress... values) {

    }

}
