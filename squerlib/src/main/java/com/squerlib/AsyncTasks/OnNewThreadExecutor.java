package com.squerlib.AsyncTasks;

import android.support.annotation.NonNull;

import java.util.concurrent.Executor;

/**
 * Created by Thatsjm4u on 9/24/2017.
 */

public class OnNewThreadExecutor implements Executor {
    @Override
    public void execute(Runnable Task) {
        new Thread(Task).start();
    }
}
