package com.squerlib.standard;

import android.os.Bundle;
import com.squerlib.Realm.DBEntityPK;
import com.squerlib.Realm.DBUtils;
import com.squerlib.Realm.SquerCriteria;

import java.util.ArrayList;

/**
 * Created by Thatsjm4u on 10/13/2017.
 */

public abstract class AbstractDBEntityPickerActivity<T extends DBEntityPK> extends AbstractPickerActivity {

    private DBUtils dbUtils;

    /**
     * @Return Return a Selection List
     */
    @Override
    protected ArrayList getitemList() {
        if(putSquerCriteriaFirst() != null)
            return getDbUtils().findSorted(putSquerCriteriaFirst());
        else
            return new ArrayList<>();
    }

    protected DBUtils getDbUtils() {
        if(null != dbUtils)
            dbUtils = new DBUtils();
        return dbUtils;
    }

    /**
     * @Return Return a Selection Criteria {@link SquerCriteria}
     * with which parent Class execute criteria and show the selection list
     * */
    protected abstract SquerCriteria putSquerCriteriaFirst();

}
