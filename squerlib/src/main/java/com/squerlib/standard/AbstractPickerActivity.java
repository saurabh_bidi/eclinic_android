package com.squerlib.standard;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.squerlib.AsyncTasks.OnNewThreadExecutor;
import com.squerlib.CustomAdapters.GenericListAdapter;
import com.squerlib.R;
import com.squerlib.Realm.DBEntity;
import com.squerlib.Realm.DBEntityPK;
import com.squerlib.Realm.DBUtils;
import com.squerlib.Realm.SquerCriteria;
import com.squerlib.session.SquerSession;
import com.squerlib.session.SquerSessionManager;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Thatsjm4u on 9/29/2017.
 */

public abstract class AbstractPickerActivity<T> extends AbstractListActivity<T> {

    private Serializable mPickedItem;
    private boolean mItemPicked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * @Return Return a Selection Criteria {@link SquerCriteria}
     * with which parent Class execute criteria and show the selection list
     * */
    protected abstract SquerCriteria putSquerCriteriaFirst();

    protected abstract ListAdapter setinitialAdapter();

    protected void setItemClickListenerForResult() {
        getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                if(getListView().getAdapter() !=null){
                    mPickedItem = (Serializable) getListView().getAdapter().getItem(position);
                    mItemPicked = true;
                    finish();
                }
            }
        });
    }

    @Override
    public boolean onNavigateUp() {
        mItemPicked = false;
        finish();
        return super.onNavigateUp();
    }

    @Override
    public void finish() {
        if(mItemPicked){
            Intent resultIntent = getIntent();
            resultIntent.putExtra("picked_item", mPickedItem);
            setResult(RESULT_OK, resultIntent);
        }
        super.finish();
    }

    @Override
    public void finishActivity(int requestCode) {
        if(mItemPicked){
            Intent resultIntent = getIntent();
            resultIntent.putExtra("picked_item", mPickedItem);
            setResult(RESULT_OK, resultIntent);
        }
        super.finishActivity(requestCode);
    }

}
