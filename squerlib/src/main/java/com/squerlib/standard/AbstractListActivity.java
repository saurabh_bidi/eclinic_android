package com.squerlib.standard;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.widget.ListView;
import com.squerlib.CustomAdapters.GenericListAdapter;
import com.squerlib.R;
import java.util.ArrayList;

/**
 * Created by Thatsjm4u on 10/4/2017.
 */

public abstract class AbstractListActivity<T> extends AbstractAsyncActivity {

    private ListView listView;
    private ListAdapter listAdapter;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(setLayout());
        listView = (ListView) findViewById(R.id.picker_list);
        showLoadingProgressDialog();
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                listAdapter = setinitialAdapter();
                listView.setAdapter(listAdapter);
                dismissProgressDialog();
            }
        };

        Handler handler1 = new Handler(Looper.getMainLooper());
        handler1.post(new Runnable() {
            @Override
            public void run() {
                listAdapter.clearItemList();
                listAdapter.addItems(getitemList());
                handler.sendEmptyMessage(0);
            }
        });
//        new OnNewThreadExecutor().execute(new Runnable() {
//            @Override
//            public void run() {
//
//            }
//        });
    }

    /** Layout Must Contains ListView Having id as '@+id/picker_list' */
    protected int setLayout() {
        return R.layout.activity_picker;
    }

    /**
     * @Return Return a Selection List,
     *
     * <b> Precaustions </b> <br/>
     * Don't use to manipulate itemList on any thread or handler,
     * A dedicated Handler is already serving this code.
     * item list can't be NULL,
     * */
    protected abstract ArrayList<T> getitemList();

    protected abstract ListAdapter setinitialAdapter();

    public ListView getListView() {
        return listView;
    }

    public ListAdapter getListAdapter() {
        return listAdapter;
    }

    public void resetListAdapter(ListAdapter listAdapter) {
        this.listAdapter = listAdapter;
        listView.setAdapter(this.listAdapter);
    }

    protected abstract class ListAdapter extends GenericListAdapter<T> {

        public ListAdapter() {
            super(getitemList());
        }
    }

}
