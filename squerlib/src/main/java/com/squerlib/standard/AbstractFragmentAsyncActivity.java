package com.squerlib.standard;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;
import android.widget.Toast;
import com.squerlib.R;

import java.util.Calendar;
import java.util.TimeZone;

public abstract class AbstractFragmentAsyncActivity extends FragmentActivity implements AsyncActivity {

    protected static final String TAG = AbstractAsyncActivity.class.getSimpleName();
    private AlertDialog tzDialog = null;
    private boolean invalidTx = false;
    private Bundle savedInstanceState;
    private ProgressDialog progressDialog;
    private boolean destroyed = false;
    private boolean exit = false;

    // ***************************************
    //                                          Activity methods
    // ***************************************
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);

    	//opening transition animations
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }
    
    @Override
	protected void onPause() {
		super.onPause();
		//closing transition animations
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
	}
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        destroyed = true;
    }
    
    @Override
	public void finish() {
		// TODO Auto-generated method stub
		super.finish();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
	}
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
	public boolean onNavigateUp() {
		finish();
		return super.onNavigateUp();
	}

    // ***************************************
    //                                          Public methods
    // ***************************************
    public void showLoadingProgressDialog() {
        this.showProgressDialog("Loading. Please wait...");
    }

    public void showProgressDialog(CharSequence message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setIndeterminate(true);
        }

        progressDialog.setMessage(message);
        progressDialog.show();
    }

    public void dismissProgressDialog() {
        if (progressDialog != null && !destroyed) {
            progressDialog.dismiss();
        }
    }

    public void toast(String message) {
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    public Bundle getSavedInstanceState() {
        return savedInstanceState;
    }

    public void setSavedInstanceState(Bundle savedInstanceState) {
        this.savedInstanceState = savedInstanceState;
    }
    
	@Override
	public void showLoadingProgressDialog(boolean closeable) {
		this.showLoadingProgressDialog();
		progressDialog.setCancelable(closeable);
	}

	@Override
	public void showProgressDialog(CharSequence message, boolean closeable) {
		this.showProgressDialog(message);
		progressDialog.setCancelable(closeable);
	}

	@Override
	public void updateProggressDialogMessage(String msg){
		if(progressDialog!=null && !destroyed) progressDialog.setMessage(msg);
	}

    public void checkDateTime() throws Exception {

        if(!AbstractFragmentAsyncActivity.isValidTimeZone(getContentResolver())){
            invalidTx = true;
            tzDialog = new AlertDialog.Builder(this)
                    .setTitle("Alert")
                    .setCancelable(false)
                    .setMessage("Your device timezone and time is not set to sync automatically. Your data will not be synced with the server till the timezone and time are set correctly")
                    .show();
            return;
        }
        if(invalidTx) {
            tzDialog.hide();
            tzDialog.dismiss();
        }
    }

    public static boolean isValidTimeZone(ContentResolver resolver) throws Exception{
        int timeSynced = Settings.System.getInt(resolver, Settings.System.AUTO_TIME,0);
        Calendar cal = Calendar.getInstance();
        TimeZone tz = cal.getTimeZone();
        Log.d("Time zone","="+tz.getDisplayName());
        String currenttz = tz.getID();
        int tzSynced=0;
        if (currenttz.equals("Asia/Calcutta") || currenttz.equals("Asia/Katmandu")
                ||currenttz.equals("Asia/Kathmandu")||currenttz.equals("Asia/Kolkata")
                ||Settings.System.getInt(resolver, Settings.System.AUTO_TIME_ZONE,0)==1)
            tzSynced=1;
        return timeSynced==1 && tzSynced==1;
    }

    public void doublePressToExit() {
        if (exit)
            AbstractFragmentAsyncActivity.this.finish();
        else {
            toast("Press Back again to Exit");
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }
    }

}
