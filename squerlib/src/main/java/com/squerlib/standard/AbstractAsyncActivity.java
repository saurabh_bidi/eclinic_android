package com.squerlib.standard;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;
import com.squerlib.R;
import com.squerlib.SquerApplication;
import com.squerlib.utils.LogUtils;
import java.util.ArrayList;
import java.util.List;

public class AbstractAsyncActivity extends AppCompatActivity implements AsyncActivity {

    protected static final String TAG = LogUtils.makeLogTag(AbstractAsyncActivity.class);

    private Bundle savedInstanceState;

    private ProgressDialog progressDialog;

    private boolean destroyed = false;
    public static Location mlocation;
    private Toolbar toolbar;

    // ***************************************
    // Activity methods
    // ***************************************
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
//    	int actionBarTitle = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
//		TextView actionBarTitleView = (TextView) getWindow().findViewById(actionBarTitle);
//
//        if (actionBarTitleView != null) {
//            actionBarTitleView.setTypeface(TypefaceLoader.getTypeface(FontFaceCode.FONT_BOLD.fontFaceCode()));
//            if(!TextUtils.isEmpty(actionBarTitleView.getText())){
//            	actionBarTitleView.setText(actionBarTitleView.getText().toString().toUpperCase());
//            }
//        }
//
//        int actionBarSubTitle = Resources.getSystem().getIdentifier("action_bar_subtitle", "id", "android");
//        TextView actionBarSubTitleView = (TextView) getWindow().findViewById(actionBarSubTitle);
//
//		if (actionBarSubTitleView != null) {
//            actionBarSubTitleView.setTypeface(TypefaceLoader.getTypeface(FontFaceCode.FONT_MEDIUM.fontFaceCode()));
//        }
    	//opening transition animations
        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
    }

    @Override
    public void setToolbar(int ToolbarId) {
        toolbar = (Toolbar) findViewById(ToolbarId);
        setSupportActionBar(toolbar);
    }

    /** uses default Toolbar id : '@+id/toolbar' , as if you included the layout Squerlib.R.layout.tool_bar in yout current layout */
    protected void setToolbar() {
        setToolbar(R.id.toolbar);
    }
    
//    @Override
//	protected void onPause() {
//		super.onPause();
//        SquerApplication.getInstance().setUiInForeground(false);
//		//closing transition animations
//        overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
//	}


    @Override
    protected void onDestroy() {
        super.onDestroy();
        destroyed = true;
    }
    
    @Override
	public void finish() {
		// TODO Auto-generated method stub
		super.finish();
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		finish();
	}
	
	@Override
	public boolean onNavigateUp() {
		finish();
		return super.onNavigateUp();
	}

    // ***************************************
    // Public methods
    // ***************************************
    @Override
    public void showLoadingProgressDialog() {
        this.showProgressDialog("Loading. Please wait...");
    }

    @Override
    public void showProgressDialog(CharSequence message) {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setIndeterminate(true);
        }

        progressDialog.setMessage(message);
        progressDialog.show();
    }

    @Override
    public void dismissProgressDialog() {
        if (progressDialog != null && !destroyed) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void toast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

//    public void snackbar(String message) {
//        Snackbar.make(this.getWindow().getDecorView(), message, Snackbar.LENGTH_LONG).setAction("Action", null).show();
////        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
//    }

    @Override
    public Bundle getSavedInstanceState() {
        return savedInstanceState;
    }

    @Override
    public void setSavedInstanceState(Bundle savedInstanceState) {
        this.savedInstanceState = savedInstanceState;
    }

	@Override
	public void showLoadingProgressDialog(boolean closeable) {
		this.showLoadingProgressDialog();
		progressDialog.setCancelable(closeable);
	}

	@Override
	public void showProgressDialog(CharSequence message, boolean closeable) {
		this.showProgressDialog(message);
		progressDialog.setCancelable(closeable);
	}
	
	@Override
	public void updateProggressDialogMessage(String msg){
		if(progressDialog!=null && !destroyed) progressDialog.setMessage(msg);
	}

    public boolean requestPermissions(String permissionType, int MY_PERMISSIONS_REQUEST, String ContentToShow) {

        if (ContextCompat.checkSelfPermission(this, permissionType)!= PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
//            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permissionType)) {
//
//                    toast(ContentToShow);
//                // Show an explanation to the user *asynchronously* -- don't block
//                // this thread waiting for the user's response! After the user
//                // sees the explanation, try again to request the permission.
//
//            } else
                {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{permissionType},
                        MY_PERMISSIONS_REQUEST);
                return false;
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callbackSuccess method gets the
                // result of the request.
            }
        }
        else
            return true;
    }

    @Override
    public boolean isNetworkAvailable() {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    /**
     * Dispatch onResume() to fragments.  Note that for better inter-operation
     * with older versions of the platform, at the point of this call the
     * fragments attached to the activity are <em>not</em> resumed.  This means
     * that in some cases the previous state may still be saved, not allowing
     * fragment transactions that modify the state.  To correctly interact
     * with fragments in their proper state, you should instead override
     * {@link #onResumeFragments()}.
     */
//    @Override
//    protected void onResume() {
//        super.onResume();
//        SquerApplication.getInstance().setUiInForeground(true);
//    }

//    public void getLocation() {
//        SmartLocation.with(getApplicationContext()).location()
//                .oneFix()
//                .start(new OnLocationUpdatedListener() {
//                    @Override
//                    public void onLocationUpdated(Location location) {
//                        mlocation = location;
////                        AbstractAsyncActivity.this.dismissProgressDialog();
//                    } });
//    }

    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;

    public boolean checkAndRequestPermissions() {
        int camera = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int storage = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int loc = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION);
        int loc2 = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
        int phone_state = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camera != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (storage != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (loc2 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (loc != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (!listPermissionsNeeded.isEmpty())
        {
            ActivityCompat.requestPermissions(this,listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]),REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {
                        for(int i: grantResults)
                        {
                            if(i == PackageManager.PERMISSION_DENIED)
                            {
                                new AlertDialog.Builder(this)
                                        .setTitle("Allow Permissions.")
                                        .setMessage("It seems that you had missed some permission,\nPlease click on settings & then turn on all permissions for successfully saving & uploading content.")
                                        .setPositiveButton("Permission Setting", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                Intent intent = new Intent();
                                                intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                                Uri uri = Uri.fromParts("package", getPackageName(), null);
                                                intent.setData(uri);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                                                intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
                                                startActivity(intent);
                                            }
                                        })
                                        .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                dialogInterface.dismiss();
                                            }
                                        })
                                        .create().show();
                                break;
                            }
                        }
            }
        }

    }

}
