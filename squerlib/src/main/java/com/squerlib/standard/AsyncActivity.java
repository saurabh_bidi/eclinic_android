package com.squerlib.standard;

import android.os.Bundle;

public interface AsyncActivity {

    void setToolbar(int ToolbarId);

    public void showLoadingProgressDialog();

    public void showProgressDialog(CharSequence message);

    Bundle getSavedInstanceState();

    void setSavedInstanceState(Bundle savedInstanceState);

    public void showLoadingProgressDialog(boolean closeable);

    public void showProgressDialog(CharSequence message, boolean closeable);

    public void dismissProgressDialog();
    
    public void toast(String message);

	void updateProggressDialogMessage(String msg);

    boolean isNetworkAvailable();
}
