package com.squerlib.session;

/**
 * Created by Thatsjm4u on 9/21/2017.
 */

public interface UserSessionEntity {

    String getCertificate();
    String getTenantId();
}
