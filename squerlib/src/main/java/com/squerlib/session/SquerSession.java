package com.squerlib.session;

import com.squerlib.UserParentEntity;

import java.util.HashMap;
import java.util.Map;

public abstract class SquerSession<U> {

    protected Map<String,Object> mSessionData = new HashMap<>();
    protected U mUser;

	public U getUser() {
        return mUser;
    }

	public void setUser(U user) {
        mUser = user;
    }

	public abstract String getAuthToken();

    public Object getmSessionData(String key) {
        return mSessionData.get(key);
    }

    public void setmSessionData(String key,Object value) {
       mSessionData.put(key,value);
    }

    public void clearMapValue(String key){
        mSessionData.remove(key);
    }

    public void clearMap() {
        mSessionData.clear();
    };
}
