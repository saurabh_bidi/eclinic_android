package com.squerlib.session;

import android.content.Context;
import com.squerlib.session.SquerSession;

import java.util.HashMap;

import io.realm.Realm;

/**
 * Created by pulkit on 05/03/16.
 */
public interface SquerSessionManager {

    /**
     * @param context
     * @return
     * @inheritDoc
     * What To DO?
     *   get the instance of class<? extends SquerSession>
     *       & check wether the user exist in session or not if not than set
     *        User of Class<? extends UserParentEntity> if you have Database,
     *
     *        if you dont have DataBase just manage the user Object null;
     *
     */
    public abstract <Ss extends SquerSession> Ss getSquerSession(Context context);

}
