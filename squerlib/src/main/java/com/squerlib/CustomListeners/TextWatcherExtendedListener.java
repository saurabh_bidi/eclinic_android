package com.squerlib.CustomListeners;

import android.text.Editable;
import android.text.NoCopySpan;
import android.view.View;

/**
 * Created by Thatsjm4u on 10/26/2017.
 */

public interface TextWatcherExtendedListener extends NoCopySpan
{
    public void afterTextChanged(View v, Editable s);

    public void onTextChanged(View v, CharSequence s, int start, int before, int count);

    public void beforeTextChanged(View v, CharSequence s, int start, int count, int after);

    void afterTextChanged(Editable editable);
}