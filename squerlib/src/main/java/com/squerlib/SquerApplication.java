package com.squerlib;

import android.app.Application;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

import com.facebook.stetho.Stetho;
import com.uphyca.stetho_realm.RealmInspectorModulesProvider;

import java.util.Map;

import io.realm.FieldAttribute;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;

/**
 * Created by Thatsjm4u on 9/7/2017.
 */

public abstract class SquerApplication extends Application {

    public static final String TAG = SquerApplication.class.getSimpleName();
    private static SquerApplication mInstance;
    public static boolean RealmIntialized = false;
    private boolean uiInForeground;

    public boolean isUiInForeground() {
        return uiInForeground;
    }

    public void setUiInForeground(boolean uiInForeground) {
        this.uiInForeground = uiInForeground;
    }

//    private static RestClient restClient;
//    private RealmConfiguration defaultConfig;

    public static synchronized SquerApplication getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
//        initRealm();
//        AttachStetho();
    }

    protected void AttachStethoRealm() {
        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                        .build());
    }

    protected void AttachStetho() {
        Stetho.initialize(
                Stetho.newInitializerBuilder(this)
                        .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                        .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                        .build());
    }

    protected RealmConfiguration initRealm(long DBVersion, RealmMigration migration) {
        RealmIntialized = true;
        Realm.init(this);
        return new RealmConfiguration.Builder() // Realm.getDefaultConfiguration();
                .schemaVersion(DBVersion)
                .migration(migration)
                .build();
    }

    protected void SetRealmConfig(RealmConfiguration defaultConfig) {
        Realm.setDefaultConfiguration(defaultConfig);
    }


// Create a new class
    protected void RealmClassOnMigration(RealmSchema schema, String TableName, String PrimaryKeyColumnName, Map<String, Class<?>> ColumnMap) {

        RealmObjectSchema TempRealm = schema.create(TableName);
            for(Map.Entry<String, Class<?>> entry : ColumnMap.entrySet()) {
                if (PrimaryKeyColumnName.equals(entry.getKey()))
                    TempRealm.addField(entry.getKey(), entry.getValue(), FieldAttribute.PRIMARY_KEY);
                else
                    TempRealm.addField(entry.getKey(), entry.getValue());
            }
    }


    private String getClassName (Class<?> enclosingClass) {
        if (enclosingClass != null) {
            return (enclosingClass.getName());
        } else {
            return (getClass().getName());
        }
    }

    public Object makeObject(Class<?> clazz) throws IllegalAccessException, InstantiationException, ClassNotFoundException {

        Object o = null;
            o = Class.forName(getClassName(clazz)).newInstance();
        return o;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public boolean hasPermission(String permission)
    {
        try {
            PackageInfo info = getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), PackageManager.GET_PERMISSIONS);
            if (info.requestedPermissions != null) {
                for (String p : info.requestedPermissions) {
                    if (p.equals(permission)) {
                        return true;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}
