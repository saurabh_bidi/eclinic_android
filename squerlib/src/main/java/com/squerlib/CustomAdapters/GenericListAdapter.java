package com.squerlib.CustomAdapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Thatsjm4u on 9/28/2017.
 */

public abstract class GenericListAdapter<T> extends BaseAdapter {

    private ArrayList<T> items = new ArrayList<>();
    public GenericListAdapter(ArrayList<T> items) {
        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        return getRowView(position, view, viewGroup);
    }

    public abstract View getRowView(int position, View convertView, ViewGroup viewGroup);

    @Override
    public long getItemId(int i) {
        return 0;
    }

    public ArrayList<T> getItems() {
        return items;
    }

    public void clearItemList() {
        items.clear();
    }

    public boolean deleteItem(T item) {
        return items.remove(item);
    }

    public void deleteItem(int position) {
        items.remove(position);
    }

    public void addItem(T item) {
        items.add(item);
    }

    public void addItems(ArrayList<T> headers) {
        items.addAll(headers);
    }

}
