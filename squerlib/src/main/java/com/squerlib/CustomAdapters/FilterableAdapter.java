package com.squerlib.CustomAdapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;

import java.util.List;

public class FilterableAdapter<E> extends BaseAdapter implements Filterable{
	

	protected Context context;
	protected List<E> originalData;
	protected List<E> filteredData;
	protected Filter mFilter;
	
	public FilterableAdapter(Context context, List<E> items){
		this.context = context;
		this.originalData = items;
		this.filteredData = items;
	}
	
	@Override
	public int getCount() {
		return filteredData.size();
	}

	@Override
	public E getItem(int position) {
		return filteredData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	@SuppressLint({ "InflateParams", "DefaultLocale" })
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //TODO
        return convertView;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
    
    @Override
	public Filter getFilter() {
		return mFilter;
	}
    
    public void setFilter(Filter filter){
    	mFilter = filter;
    }
	
}
